---
title: Introduction
lang: fr-FR
---

![](http://www.leegte.org/work/scrollbarcomposition-com/scrollbars-screenshot-osx-aqua.jpg)
Jan Robert [Leegte](http://www.leegte.org/), Scrollbar composition, 2000[
www.scrollbarcomposition.com](http://www.scrollbarcomposition.com/)
collection Stedelijk Museum Amsterdam / MOTI

---

> "At the same moment, they are on many other screens, surrounded by different objects, different colors, different sounds. You are seeing them in the context of your own life. They are surrounded not by gilt frames, but by the familiarity of the room you are in, and the people around you."  
> **John Berger**, [Ways of seeing](https://www.youtube.com/watch?v=0pDE4VX_9Kk)

# webb2.netlify.com

Bienvenue sur ce support du cours de web B2.

Nous parlerons ici de la différence entre l'Internet et le web, un serveur et un client, le transfert FTP, le langage HTML, les feuilles de styles \(CSS\) et éventuellement de javaScript. 

Nous verrons également comment les artistes ont, depuis le début, su s'approprier ces nouveaux langages et modes de diffusion.

L'objectif du cours est à la fois technique et artistique.

L'évaluation ici est une évaluation continue. Un exercice sera réalisé chaque semaine et rendu pour le cours suivant. Un travail de fin d'année viendra cloturer cette suite de modules.

Votre présence est bien entendu requise. Au delà de l'aspect administratif.

En plus de ces exercices hebdomadaires, un travail de fin d'année vous sera demandé. Un calendrier vous sera communiqué au cours...

Vous trouverez dans la colonne de gauche la fiche de chaque cours, laquelle comprend:

* La matière vue au cours en question
* Des ressources techniques \(pages webs, tutoriels..\)
* Des artistes de référence liés au cours
* L'exercice à rendre pour le cours suivant

---

* [Open codes, ZKM](https://open-codes.zkm.de/en/works-exhibition)
* [Materialising the Internet](http://www.mu.nl/en/exhibitions/materialising-the-internet), MU Art space, Eindhoven \(closed\)

![](http://www.mu.nl/images/pictures/materialising-the-internet-large-520230.jpg)
**Jan Robert Leegte**, Scrollbar composition, à Eindhoven en 2017