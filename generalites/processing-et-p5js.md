---
title: Processing et P5js
lang: fr-FR
---

# Processing et P5js

## Processing

Processing est **à la fois un langage et une interface** pour éditer votre code. Il s'agit d'un logiciel libre qui doit être installé sur votre machine (il l'est sur toutes celles de l'école).
Inspiré de **Design by numbers** de John Maeda (dont vous trouverez un très beau livre à la bibliothèque), Processing a vu le jour au [MIT](https://www.media.mit.edu/), en 2001.
Clairement **orienté création visuelle**, il permet d'aborder les concepts de base de la programmation (variables, boucles, conditions...) de façon relativement **simple et autonome**.

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.3rem;"><iframe src="https://player.vimeo.com/video/74725118?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.3rem;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


1. <a href="https://processing.org/download/" target="_blank">Téléchargez</a> et installez Processing
2. Ouvrez le programme
3. Editez votre code
4. Testez-le
5. Sauvez votre travail

```processing
void setup() {
    size(600,400);
}
void draw() {
    background(220);
    ellipse(mouseX,mouseY,30,30);
}
```

<a data-fancybox title="Processing" href="/assets/processing-cap.png">![Processing](/assets/processing-cap.png)</a>
<span class="legende">L'environnement de Processing.</span>

<div class="notification box-processing">
    <h4>Liens utiles</h4>
    <ul>
    <li><a href="https://processing.org/reference/" target="_blank">Documentation de Processing</a></li>
    <li><a href="http://learningprocessing.com/examples/chp03/example-03-02-mouseX-mouseY" target="_blank">Exemples</a></li>
    <li><a href="https://fr.flossmanuals.net/processing/introduction/" target="_blank">FLOSS manuals</a></li>
    </ul>
</div>

## P5js

P5js est une **bibliothèque JavaScript** -Open Source- créée par Lauren McCarthy en 2013. Elle descend (la bibliothèque, pas Lauren) directement de Processing et a été développée dans le même esprit, et avec les mêmes objectifs, mais adaptés au web d'aujourd'hui.  Les fonctions utilisées sont très proches, dans l'esprit et dans la syntaxe, de Processing. Cela signifie que vos sketchs peuvent s'exécuter **directement dans votre navigateur**.

<div style="text-align:center;margin:30px 0;">

<a data-fancybox title="p5js" href="/assets/p5schema.png">![Boucles](/assets/p5schema.png)</a>

</div>

Bien qu'une connaissance des langages HTML et CSS ainsi que des notions de base liées au web soient un plus, P5js propose un **éditeur de code en ligne** très pratique, qui vous permet de rentrer directement dans le vif du sujet sans connaissance préalable. Une fois un compte créé(Allez-y les yeux fermés. Ils ne vous spammeront pas), il vous est alors possible de sauver vos sketchs en ligne et de les **partager** directement.

Ces fonctions de sauvegarde et de partage en font un outil particulièrement intéressant.

1. RDV sur [https://editor.p5js.org/](https://editor.p5js.org/)
2. Créez un compte en ligne
3. Editez votre code
4. Partagez!

```javascript
function setup() {
    createCanvas(600,400);
}
function draw() {
    background(200);
    ellipse(mouseX,mouseY,30,30);
}
```

<a data-fancybox title="P5js" href="/assets/p5js-cap.png">![P5js](/assets/p5js-cap.png)</a>
<span class="legende">Le "même" programme avec P5js. Notez les quelques petites différences de syntaxe.</span>

<div class="notification box-p5js">
    <h4>Liens utiles</h4>
    <ul>
    <li><a href="https://p5js.org/reference/" target="_blank">Documentation de P5js</a></li>
    <li><a href="https://p5js.org/examples/" target="_blank">Exemples</a></li>
    <li><a href="https://www.youtube.com/user/shiffman/playlists?view=50&sort=dd&shelf_id=14" target="_blank">P5js en vidéo, par Daniel Shiffman</a></li>
    </ul>
</div>

## Passer de Processing à P5js

Passer un sketch Processing vers P5js est assez simple. Seuls quelques changements sont nécessaire.

La liste complète et détaillée est reprise ici:

+ [https://github.com/processing/p5.js/wiki/Processing-transition](https://www.media.mit.edu/)
+ [Cette page](http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html) reprend de façon également exhaustive les différences entre Processing et P5js ainsi que la façon de passer de l'un à l'autre.
+ [http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html](http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html)
+ [Convertisseur en ligne](http://faculty.purchase.edu/joseph.mckay/p5jsconverter.html)

------

+ http://gerard.paresys.free.fr/P/index.html
+ http://purin.co/Experiments-with-P5-js
