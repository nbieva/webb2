---
title: Organisation fin d'année
lang: fr-FR
---

# Organisation fin d'année

Vous trouverez ci-dessous les horaires de passage des remises finales du travail [Geonetart](http://www.digitalab.be/briefing-geonetart-2021/). Ces remises auront lieu à La Cambre en présentiel et non à distance.

## Remises finales

::: tip
Les remises finales auront lieu **à l'Abbaye, Salle art numérique 1 ou 2** les **jeudi 10 et vendredi 11 juin 2021**. 
:::

Vous trouverez les horaires de passage [sur cette page](https://docs.google.com/spreadsheets/d/1lW29ggwH98mL_WSjMSrsMQonkQN7uZRTNi6qSY3T4gM/edit?usp=sharing) (colonne de gauche). Vous pouvez également vous inscrire aux premières lectures sur cette même page.

Si vous préférez une autre heure que celle indiquée, merci de vous arranger pour permuter avec un.e autre étudiant.e et de **m'informer de la modification**.

Dans tous les cas, soyez-là 5 minutes avant l'heure prévue. Notez qu'il y a toujours un risque que ces horaires soient modifiés. **Les horaires seront alors mis à jour**. Vérifiez-les donc régulièrement.

## Premières lectures

::: tip
Les premières lectures auront lieu **en ligne** les **jeudi 3 et vendredi 4 juin 2021**.
:::

Ces première lectures, bien que n'étant pas obligatoires, permettent parfois d'éviter les catastrophes et vous permettent d'avoir un **retour constructif sur ce que vous avez déjà réalisé** et de **répondre à vos questions**.

Il est donc indispensable d'être déjà bien avancé dans le travail ce jour-là. Je ne sais donner de retour que sur ce que je vois. 

Merci de réserver votre créneau horaire [sur cette page](https://docs.google.com/spreadsheets/d/1lW29ggwH98mL_WSjMSrsMQonkQN7uZRTNi6qSY3T4gM/edit?usp=sharing).

Indiquez juste votre nom dans une des plages horaires disponibles. Une invitation Google Meet vous sera envoyée ensuite par email (mail de la cambre).

N'oubliez pas de relire attentivement [le briefing du travail sur digitalab.be/](http://www.digitalab.be/briefing-geonetart-2021/)

-------

Bon Jurys et bon travail à tou.te.s!

nb