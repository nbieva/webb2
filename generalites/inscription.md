---
title: Inscription
lang: fr-FR
---

# Inscriptions Code B3.

Ces workshops s'adressent aux étudiants de B3 inscrits dans un CASO arts numériques. Leur objectif est d'approfondir le travail avec le code rapidement évoqué en B1, afin d'enrichir vos projets dans vos options.

Les 3 premiers modules auront lieu les **22 et 29 novembre**, ainsi que le **6 décembre** (3 x 2h)
Le workshop de janvier aura lui lieu les **29 et 30 janvier 2020**, toute la journée (de 9 à 18h).
Les suivis de projets seront organisés ultérieurement.

::: warning
Il est impératif de venir avec votre propre ordinateur. Si cela posait un souci, merci de nous le faire savoir via le formulaire.
:::

L'inscription se fait via le formulaire ci-dessous:

<iframe style="margin-top:30px;" src="https://docs.google.com/forms/d/e/1FAIpQLSe2UCkWcGeO4QJ9SDroi0F5vZ3ZDWECIZp6cE5cuccbEnT2yQ/viewform?embedded=true" width="740" height="968" frameborder="0" marginheight="0" marginwidth="0">Chargement…</iframe>