```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Positionnements CSS</title>
        <link rel="stylesheet" media="screen" href="css/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <ul class="navigation">
            <li><a href="#">Accueil</a></li>
            <li><a href="#">A propos</a></li>
            <li><a href="#">Travaux</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
        <div class="content">
            <h1>Le Journal</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, necessitatibus non accusamus blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
            <p>Dolor sit amet, consectetur adipisicing elit. Quas delectus, necessitatibus non accusamus blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, necessitatibus non accusamus blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
            <p>Ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, necessitatibus non accusamus blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, necessitatibus non accusamus blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
        </div>
        <div class="sidebar">
            <div class="sidebar-zone">
                <h3>Un titre</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, necessitatibus non accusamus blanditiis vero facere</p>
                <ul>
                    <li><a href="#">Premier lien</a></li>
                    <li><a href="#">Deuxième lien</a></li>
                    <li><a href="#">Troisième lien</a></li>
                </ul>
            </div>
            <div class="sidebar-zone">
                <h3>Un autre titre</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, necessitatibus non accusamus blanditiis vero facere</p>
                <ul>
                    <li><a href="#">Premier lien</a></li>
                    <li><a href="#">Deuxième lien</a></li>
                    <li><a href="#">Troisième lien</a></li>
                </ul>
            </div>
        </div>
    </body>
</html>
```

