```html
<!-- Indique qu'il s'agit d'un document HTML -->
<!DOCTYPE html>
<html>
    <!-- L'entête de la page n'apparaitra pas en tant que tel dans la fenêtre du navigateur, mais elle est tout aussi importante! -->
    <head>
        <!-- La ligne suivante concerne l'encodage des caractères -->
        <meta charset="utf-8">
        <!-- Cette ligne définit le titre de votre page HTML, important pour les moteurs de recherche, et qui s'affichera notamment dans l'onglet de votre navigateur. -->
        <title>Page de test</title>
    </head>
    <!-- Le corps de la page (body) contient ce qui sera affiché à l'écran -->
    <body>
        <h1>Le Journal</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, necessitatibus non accusamus blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
    </body>
</html>
```



