```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Page de test</title>
        <link rel="stylesheet" media="screen" href="css/styles-03.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Ceci est une page de test reprenant quelques éléments HTML.">
    </head>

    <body>
        <div class="header">
            <h1>Le Journal</h1>
            <ul class="menu">
                <li><a href="#">Accueil</a></li>
                <li><a href="#">A propos</a></li>
                <li><a href="#">Actualités</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
        <div class="main-container">
            <h2>Derniers articles</h2>
            <p class="introduction">Lorem ipsum dolor sit amet, <a href="http://www.fnec.fr/" target="_blank">consectetur adipisicing</a> elit. Quas delectus, blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
            <div class="article">
                <h3>Article 1</h3>
                <img src="https://bellanyc.com/wp-content/uploads/2013/03/no.jpg">
                <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, <a href="#">necessitatibus non accusamus</a> blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
                <ul>
                    <li>
                        <a href="http://animista.net/play/text/pop-up" target="_blank">CSS animations</a>
                    </li>
                    <li>
                        <a href="[http://animista.net/play/text/pop-up](http://www.colorzilla.com/gradient-editor/)" target="_blank">Dégradés CSS</a>
                    </li>
                    <li>
                        <a href="https://css-tricks.com/almanac/" target="_blank">CSS Almanach</a>
                    </li>
                </ul>
                <p>Velit voluptas, iure voluptatibus! Aspernatur dolore ad eos voluptatum error reiciendis in aliquid esse corporis, necessitatibus eligendi eius, dicta consequuntur, facilis vitae laboriosam. Laborum laboriosam repellat iure molestias blanditiis facere.</p>
                </p>
            </div>
            <div class="article">
                <h3>Article 2</h3>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, <a href="#">necessitatibus non accusamus</a> blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
                <p>Velit voluptas, iure voluptatibus! Aspernatur dolore ad eos voluptatum error reiciendis in aliquid esse corporis, necessitatibus eligendi eius, dicta consequuntur, facilis vitae laboriosam. Laborum laboriosam repellat iure molestias blanditiis facere.</p>
                </p>
            </div>
        </div> 
        <div class="footer">
            <p>Copy copy copy right</p>
        </div>
    </body>
</html>
```



