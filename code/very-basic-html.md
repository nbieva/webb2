```html
<!DOCTYPE html><!-- Définit le type de document -->

<html>

<!-- L'entête de la page n'apparaitra pas en tant que tel dans la fenêtre du navigateur, mais elle est tout aussi importante! -->
<head>

    <!-- La ligne suivante concerne l'encodage des caractères -->
    <meta charset="utf-8">

    <!-- Cette ligne définit le titre de votre page HTML, important pour les moteurs de recherche, et qui s'affichera notamment dans l'onglet de votre navigateur. -->
    <title>Page de test</title>
</head>

<!-- Le corps de la page (body) contient ce qui sera affiché à l'écran -->

<body>
    <div>
        <h1>Le Journal</h1>
        <ul>
            <li><a href="#">Accueil</a></li>
            <li><a href="#">A propos</a></li>
            <li><a href="#">Actualités</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
    </div>
    <div>
        <h2>Derniers articles</h2>
        <p>Lorem ipsum dolor sit amet, <a href="http://www.fnec.fr/">consectetur adipisicing</a> elit. Quas delectus, blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
        <div>
            <h3>Article 1</h3>
            <img src="https://cdn.pixabay.com/photo/2013/04/06/11/50/image-editing-101040_960_720.jpg">
             <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, <a href="#">necessitatibus non accusamus</a> blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
            <p>Velit voluptas, iure voluptatibus! Aspernatur dolore ad eos voluptatum error reiciendis in aliquid esse corporis, necessitatibus eligendi eius, dicta consequuntur, facilis vitae laboriosam. Laborum laboriosam repellat iure molestias blanditiis facere.</p>
        </div>
        <div>
            <h3>Article 2</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas delectus, <a href="#">necessitatibus non accusamus</a> blanditiis vero facere, cum voluptates sequi et excepturi, esse at sint, autem deleniti aliquid itaque sed aspernatur.</p>
            <p>Velit voluptas, iure voluptatibus! Aspernatur dolore ad eos voluptatum error reiciendis in aliquid esse corporis, necessitatibus eligendi eius, dicta consequuntur, facilis vitae laboriosam. Laborum laboriosam repellat iure molestias blanditiis facere.</p>
        </div>
    </div>
    <div>
        <p>Copy copy copy right</p>
    </div>
</body>

</html>
```



