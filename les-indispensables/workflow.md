---
title: Workflow
lang: fr-FR
---




Il est certain qu'à un moment, nous allons avoir l'impression de travailler avec toute une série d'outils qu'on ne comprend pas, et ce sera effectivement le cas. Nous sommes ici orientés résultats, dans le cadre d'une recherche artistique et nous n'aurons tout simplement pas le temps de tout comprendre. Nous aurons par contre appris à apprivoiser cette part de mystère et à l'utiliser. (J'ai regardé la TV toute mon enfance sans comprendre comment fonctionnait le tube cathodique. Et je ne vous parle même pas des CD ou de la radio..).