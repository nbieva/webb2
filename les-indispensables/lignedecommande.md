---
title: La ligne de commande
lang: fr-FR
---

## Interface en ligne de commande

Une interface en ligne de commande (en anglais command line interface, couramment abrégé CLI) est une interface homme-machine dans laquelle la **communication entre l'utilisateur et l'ordinateur** s'effectue **en mode texte** :

+ l'utilisateur tape une ligne de commande, c'est-à-dire du texte au clavier pour demander à l'ordinateur d'effectuer une opération ;
+ l'ordinateur affiche du texte correspondant au résultat de l'exécution des commandes tapées ou à des questions qu'un logiciel pose à l'utilisateur.

Source: [Wikipedia](https://fr.wikipedia.org/wiki/Interface_en_ligne_de_commande)

-----

La ligne de commande peut paraître un peu nébuleuse de prime abord (et elle l'est sous certains aspects) mais elle est dans bien des cas **indispensable**, et l'on éprouve un certain plaisir à dialoguer avec la machine **sans interface graphique**. Les commandes tapées s'exécutent souvent bien plus vite qu'on ne pourrait l'imaginer et certaines de ces commandes nous permettent de **faire des choses impossibles autrement**. En bref, il faut savoir ce que c'est, et comment l'utiliser, sans pour autant en être un expert.

<a data-fancybox title="Le terminal" href="/assets/terminal.png">![Terminal](/assets/terminal.png)</a>

Vous croiserez régulièrement dans des tutoriels des commandes

Le terminal

## Commandes utiles

### mkdir

Utilisez la commande <code>mkdir</code> (make directory) pour créer un répertoire.
 
```bash
mkdir monrepertoire
```

### cd

```bash
cd monrepertoire
```

https://computers.tutsplus.com/fr/tutorials/10-terminal-commands-that-every-mac-user-should-know--mac-4825

rm

sudo

## Github

