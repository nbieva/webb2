---
title: Node JS
lang: fr-FR
---

Vous trouverez ici une vidéo de Daniel Shiffman à propos de Nodejs, où il reprend également les notions de serveur, ligne de commande etc... Cette vidéo est en anglais, mais sous-titrée en français [https://www.youtube.com/watch?v=RF5_MPSNAtU](https://www.youtube.com/watch?v=RF5_MPSNAtU)

## Qu'est-ce que Node.js?

<img src="/assets/nodejs.png" style="float:right;width:200px;margin:0 0 1rem 1.5rem;">[Node.js](https://fr.wikipedia.org/wiki/Node.js) est un environnement JavaScript qui s'exécute sur votre machine (s'il est installé). Historiquement, JavaScript s'exécutait plutôt dans votre navigateur. NodeJS permet de profiter de JavaScript côté serveur également. Il utilise le [moteur de rendu javascript](https://fr.wikipedia.org/wiki/Moteur_JavaScript) V8, opensource, développé par Google et intégré entre autres au navigateur Chrome.

Pas besoin de retenir tout cela pour l'instant. Quand vous en aurez besoin, vous le saurez...

> Concrètement, Node.js est un environnement bas niveau permettant l’exécution de JavaScript côté serveur. (Wikipedia)

+ [Installation de Node](https://openclassrooms.com/fr/courses/1056721-des-applications-ultra-rapides-avec-node-js/1056956-installer-node-js)


## NPM - Node Package Manager

<img src="/assets/npm.png" style="float:right;width:130px;margin:0 0 1rem 1.5rem;">**[npm](https://www.npmjs.com/) est un gestionnaire de paquets pour Node.js**. Il vous permet, via des commandes simples dans le terminal, d'installer des fonctionnalités supplémentaires à votre environnement de développement javascript. On peut l'utiliser par exemple pour gérer dans un projet à la fois P5js, SASS, Live Server etc...

Ces paquets sont alors des **dépendances de votre projet**, puisque sans eux, il ne fonctionnera pas. Un projet web peut rassembler un très grand nombre de dépendances, chacune devant être mise à jour quand cela est nécessaire, d'où l'intérêt de gestionnaires de paquets comme **npm** ou **Yarn** (voir ci-dessous)

[P5js](https://www.npmjs.com/package/p5) s'installerait simplement dans un projet en utilisant la commande suivante à partir de la racine du dossier de mon projet:

```javascript
npm install p5

// ou

npm i p5
```

Simple, non?

## P5-manager

Un autre exemple de l'utilité de la ligne de commande, npm et node, est ce petit projet qu'est le p5-manager qui vous permet de mettre en place votre projet via cette simple commande. Prenez le temps de lire la **documentation**.

Installez-le sur votre machine (-g = globally)

```javascript
npm install -g p5-manager
```

Ensuite, vous allez dans le dossier dans lequel vous voulez créer le projet (via la command cd) et, dans le terminal, vosu tapez:

```javascript
p5 generate --bundle lenomdevotreprojet
```
