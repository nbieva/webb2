---
title: Git, GitLab, Github
lang: fr-FR
---

## La gestion de version avec GIT

<img src="/assets/gitlogo.png" style="float:right;width:150px;margin:0 0 1.2rem 1.8rem;">Si vous travaillez sur de plus importants projets, ou si vous collaborez avec d'autres personnes, ou dans tous les autres cas d'ailleurs, vous vous frotterez à un moment ou à un autre à un **outil de gestion de versions** comme [git](https://fr.wikipedia.org/wiki/Git). Dans vos recherches sur le web, vous serez d'ailleurs régulièrement amenés à consulter des dépôts (repositories) sur [Github](https://github.com/), [Gitlab](https://about.gitlab.com/), [Bitbucket](https://bitbucket.org/) ou [autres](https://git-scm.com/download/gui/windows)..

Git n'est pas évident à prendre en main mais, parfois, il s'impose et on ne peut pas faire sans. Pour l'instant, **vous devez savoir ce que sont Git et Github** par exemple, **et à quoi il servent**. la prise en main et la maîtrise des outils viendront probablement plus tard.

<a data-fancybox title="P5js" href="/assets/git.png">![P5js](/assets/git.png)</a>

## Github, GitLab, Bitbucket ..

Ces différents sites, que vous croiserez régulièrement, sont des lieux où vous pouvez, àl'instar de milliers de personnes, **héberger le code de vos projets** dans ce que l'on appelle des **dépôts (repositories)**.

Vous pouvez également **cloner** les dépôts d'autres développeurs ou artistes sur votre propre machine.

<a data-fancybox title="P5js" href="/assets/gitlab.png">![P5js](/assets/gitlab.png)</a>

### Sourcetree

+ https://www.sourcetreeapp.com/

<a data-fancybox title="P5js" href="/assets/sourcetree.png">![P5js](/assets/sourcetree.png)</a>