---
title: La documentation
lang: fr-FR
---

## Markdown

**Markdown** : [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

<img src="/assets/markdown.png" style="float:right;width:150px;margin:0 0 1.2rem 1.8rem;">Markdown est un langage de balisage léger créé en 2004 par John Gruber avec l'aide d'Aaron Swartz. Son but est d'offrir une **syntaxe facile à lire et à écrire**. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières.

Un document balisé par **Markdown peut être converti en HTML, en PDF ou en d'autres formats**.

Source: [Wikipedia](https://fr.wikipedia.org/wiki/Markdown)

En outre, il est souvent utilisé par les générateurs de sites statiques tels Jekyll, Gatsby, Hugo, Nuxt ou VuePress..

Mais c'est surtout **LE langage de la documentation de projets**. Les dépôts Git que vous visiterez sur Github, GitLab ou autres, utilisent tous les Makdown.

Il est:

+ **Très simple** à prendre en main
+ **Focus sur le contenu**
+ **Exports** HTML, PDF..
+ **Extensible**

### Liens:

+ **MacDown**: [https://macdown.uranusjr.com/](https://macdown.uranusjr.com/)




