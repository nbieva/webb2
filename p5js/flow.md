---
title: Program flow
lang: fr-FR
---


+ https://p5js.org/learn/program-flow.html

# Textile

+ Créer une interface
+ Importer des éléments
+ Les faire tourner
+ Modifier leur échelle, agrandir, rapetissir
+ modifier la couleur
+ Modifier l'opacité
+ Ajouter un contour
+ Modifier l'épaiseeur et la couleur du contour