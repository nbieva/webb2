module.exports = {
    title: 'Web B2',
    description: 'Support en ligne pour le cours de web B2',
    head: [
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js' }],
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i&display=swap' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css2?family=Atkinson+Hyperlegible:ital,wght@0,400;0,700;1,400;1,700&display=swap' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: '/assets/styles.css' }]
  ],
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        nav: [
          { text: 'Home', link: '/' },
            { text: 'Digitalab', link: 'http://digitalab.be' },
            { text: 'Inscriptions', link: '/general/formulaire' }
        ],
        sidebarDepth: 0,
        sidebar: [
            {
              title: 'Généralités',
              collapsable: false,
              children: [
                '/',
                'generalites/introduction'
              ]
            },
            {
              title: 'HTML/CSS/JS',
              collapsable: false,
              children: [
                'cours/cours1',
                'general/html2',
                'general/html3',
                'general/cours4',
                'general/cours5',
                'general/cours6',
                'general/cours7',
                'general/cours8',
                'general/forms',
                'general/javascript',
                'general/wordpress2'
              ]
            },
            /*{
              title: 'Code snippets',
              collapsable: false,
              children: [
                'code/ultra-basic-html',
                'code/very-basic-html',
                'code/css-zen-garden',
                'code/html-avec-classes',
                'code/albers',
                'code/flexbox'
              ]
            },*/
            {
              title: 'Exercices',
              collapsable: false,
              children: [
                'exercices/exercice1',
                'exercices/exercice2',
                'exercices/exercice-3',
                'exercices/exercice4',
                'exercices/exercice5',
                'exercices/albers',
                'exercices/formulaires'
              ]
            },
            {
              title: 'Les satellites',
              collapsable: true,
              children: [
                'les-indispensables/console',
                'les-indispensables/git'
              ]
            },
          ]
    }
}