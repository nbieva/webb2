---
title: Structure d'un document HTML
lang: fr-FR
---

+ [Decodelia](https://vimeo.com/157337087)

---

> The single most important thing you need in order to have a career in the arts is persistence. The second most important thing you need is talent. The third most important thing is a grounding in how the online world works. Its that Important.
>
> **Cory Doctorow**
> from [Information Doesn't Want to Be Free: Laws for the Internet Age.](https://store.mcsweeneys.net/products/information-doesn-t-want-to-be-free) 2014 \(in [Net art && cultures](http://netart.rocks/syllabus)\)

## Introduction

* [Présences, listes, rendus](https://docs.google.com/spreadsheets/d/1iXzqjjVCVu72GeBNICipYdkHFgQpsZ0bAU8CfcPUXBE/edit?usp=sharing)
* Planning (xr6, travail de fin de quadri..)

----

![](https://cdn.tutsplus.com/webdesign/uploads/legacy/articles/101_history/tutimages/first-web-browser.png)

Le premier navigateur web \(1990\), développé au CERN par [Tim Berners Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee), et utilisé par lui-même et son collègue [Robert Cailliau](https://fr.wikipedia.org/wiki/Robert_Cailliau).

+ [Une brève histoire du web](https://home.cern/fr/science/computing/birth-web/short-history-web)
+ [Le premier site web](http://info.cern.ch/hypertext/WWW/TheProject.html)
+ [Le premier navigateur](https://worldwideweb.cern.ch/browser/) ([Link](https://worldwideweb.cern.ch/))

## Au programme

+ Internet et World Wide Web, [origines](https://www.youtube.com/watch?v=8LFifvd1Rsk) - [Réseau physique](https://www.submarinecablemap.com/) ou [ceci](https://submarine-cable-map-2018.telegeography.com/) +  [Landscape with a Ruin](http://www.evan-roth.com/work/landscape-with-a-ruin/)
+ Qu'est-ce qu'un site web? On se souvient de [ceci](https://b1.netlify.app/assets/assemblage02.png)? (autre exemple [ici](http://localhost:8080/html/html3.html))
+ Navigateurs, [serveurs, clients](https://lh3.googleusercontent.com/proxy/_sZ9ND5vx4GaYjvHdD6as43dYOTSlxfT3DRi75BNhpzCSsOpHfP4_4-n_lk9Fxe62Tn7lm6htx9hLFKyYdkShHyG92scDHyTp2NKDFOiKRj6CwJFntX6biVk3OsbOu426X1OS2wv), [adresse IP](https://whatismyipaddress.com/fr/mon-ip), nom de domaine, hébergeurs...
+ [http: et file:](https://developer.mozilla.org/fr/docs/Apprendre/Ouvrir_un_fichier_dans_un_navigateur_web)
+ Les différents langages que nous allons aborder \(HTML, CSS, javaScript, ...\)
+ Les langages de balisage \(html, wikimedia, markdown, [SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics)...\) et autres \(programmation\).
+ Nos outils
+ Introduction au [langage HTML](https://www.w3schools.com/html/html_basic.asp) \(le code n'est [jamais très loin](https://www.wikihow.com/View-Source-Code)...\)
+ HTML - CSS - JavaScript
+ **Olia Lialina**, Summer, 2013 - [http://kimasendorf.com/olia/summer/](http://kimasendorf.com/olia/summer/)

![](https://media0.giphy.com/media/Kd8QOZ5WrjSmY/source.gif)

## Nos outils

+ Le **minimum** : Votre navigateur préféré \(Firefox ou Chrome sont excellents\)
+ Les différents éditeurs disponibles : [VScode](https://code.visualstudio.com/), [Sublime text](https://www.sublimetext.com/), [Atom](https://atom.io/), [Brackets](http://brackets.io/), ...
+ [Themes](https://vscodethemes.com/) (Cmd + K + T)
+ Les extensions importantes ([Live server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer), ...)
+ Le **presque indispensable**: l'[inspecteur web](https://developer.mozilla.org/fr/docs/Outils/Inspecteur)
+ Eventuellement un éditeur en ligne: [https://codesandbox.io/](https://codesandbox.io/), [Codepen](https://codepen.io/), [jsfiddle](https://jsfiddle.net/), l'[éditeur de P5](https://editor.p5js.org/) etc. qui peut faire office de bac à sable.
+ **[Netlify](https://www.netlify.com/)**, que nous utiliserons souvent pour déployer nos pages sur le réseau.
+ L'architecture d'un projet
+ **FileZilla** que vous utiliserez pour le transfert FTP.

![](https://mdn.mozillademos.org/files/7747/inspector-color-picker.png)

## HTML: Structure et balises

* Principes généraux du langage HTML
* Structure générale
* Doctype ( + ! )
* Balises de **1er niveau** : html, head, body et balises **metas** \(et leur impact sur le SEO notamment\)
* Balises d'**entête** \(title, meta, style, script..\)
* Balises de type **block** : header, footer, h1, h2, ... , ul, ol, li, div, p, blockquote, img, table, form, canvas..
* Balises de type **inline** : span, a, strong, em
* Balises orphelines \(hr, img..\)
* Les [attributs](http://netart.rocks/notes/html)
* Les commentaires

> Listes des [principales balises HTML](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3/memento-des-balises-html)

![](http://netart.rocks/images/html3.gif)Gif: [http://netart.rocks/notes/html](http://netart.rocks/notes/html)

## Ressources

+  [**Les origines du World Wide Web**](https://www.youtube.com/watch?v=8LFifvd1Rsk)**, par Robert Cailliau**
+ **Ai WeiWei** et **Olafur Eliasson** et leur projet [Moon](https://www.moonmoonmoonmoon.com/)

## Références

* Jasper Elings
* Jan Robert Leegte, [Untitled mountains](http://www.untitledmountains.com/)
* Cory Arcangel, [Working on My Novel](https://twitter.com/WrknOnMyNovel)
* Melanie Hoff, [Hacking the browser](http://www.artdelicorp.com/category/hacking-the-browser/)
* Rafael Rozendaal, [Manual sequence](http://www.manualsequence.com/)
* Jenny Odell, [Satellite collections](http://www.jennyodell.com/satellite.html)
* [Net.art](https://fr.wikipedia.org/wiki/Net.art)
* Philippe De Jonckheere, [Le désordre](http://www.desordre.net/)
* Jonas Lund, [Critical Mass](https://critical-mass.network/about)
* [http://anthology.rhizome.org/](http://anthology.rhizome.org/)
* Olia Lialina \(Récit, Narration, texte, hypertexte, oeuvre expérience, interactivité\)
* Rafael Rozendaal, [Falling Falling](http://www.fallingfalling.com/), [RRFood](https://twitter.com/rrfood), [Midnight moment](http://www.newrafael.com/times-square-midnight-moment/)
* Les artistes qui utilisent le contexte du web
* Alexei Shulgin, [Form Ar](http://anthology.rhizome.org/form-art)t
* [Jan Robert Leegte](http://www.leegte.org/)

## Autres ressources

* [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [Webdesign en 4 minutes](https://jgthms.com/web-design-in-4-minutes/)
* [http://www.evolutionoftheweb.com/](http://www.evolutionoftheweb.com/)
* [http://www.camilleroux.com/2008/07/21/histoire-et-avenir-du-web-ebook-pdf](http://www.camilleroux.com/2008/07/21/histoire-et-avenir-du-web-ebook-pdf)
* [http://www.w3.org/MarkUp/tims\_editor](http://www.w3.org/MarkUp/tims_editor)
* [http://hakim.se/experiments](http://hakim.se/experiments)
* [http://thecodeplayer.com/](http://thecodeplayer.com/)
* [http://leaverou.github.com/animatable/](http://leaverou.github.com/animatable/)