
#Horaires des remises finales du 13 juin

Vous trouverez ci-dessous les horaires de passage des remises finales qui auront lieu à l'Abbaye, Salle art numérique 1 ou 2.
Veillez à relire attentivement le briefing du travail sur digitalab.be, tester vos sites en ligne et avoir avec vous les fichiers de votre travail lors de la remise.

briefing: http://www.digitalab.be/briefing-geonetart/

Dans tous les cas, soyez-là 5 minutes avant l'heure prévue.


**IMPORTANT :** Les étudiants non présents dans cette liste et désirant passer les évaluations de juin pour ce cours peuvent me contacter directement par mail.