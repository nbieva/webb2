---
title: Formulaires (suite)
lang: fr-FR
---

# Formulaires + Media Queries



## Aujourd'hui

+ Vos questions sur le travail de fin d'année **Geonetart**
+ **Icônes**: Les vôtres, [Font Awesome](https://fontawesome.com/) et [Material icons](https://fonts.google.com/icons)
+ **Formulaires** HTML
+ L'[élément input](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input)
+ L'élément [textarea](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Textarea)
+ Attributs: required, placeholder, disabled, autofocus
+ Styler un formulaire
+ Intégration d'un formulaire (Ex [Google forms](https://docs.google.com/forms/u/0/))
+ Un service pour [envoyer les données de notre formulaire](https://formsubmit.co/) que l'on peut utiliser dans nos templates statiques.
+ Ou, encore plus simple si l'on utilise Netlify, utilisez [Netlify Forms](https://www.netlify.com/products/forms/), que vous trouverez dans l'interface de votre site sur Netlify.

## Websites

+ [https://brutalistwebsites.com/](https://brutalistwebsites.com/)
+ [Désordre - Philippe De Jonckheere](https://www.desordre.net/)
+ [Alexei Shulgin](http://variants.artbase.rhizome.org/Q1249/) + Le travail sur [Rhizome.org](https://rhizome.org/art/artbase/artwork/form-art/)
+ [Michael Manning, Floating form](http://www.floatingform.net/)

### Formulaires

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Formulaires HTML</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <form name="monformulaire" netlify>
            <label for="nom">Votre nom</label>
            <input type="text" name="nom" placeholder="Inscrivez ici votre nom" required>
            <label for="email">Votre email</label>
            <input type="email" name="email" placeholder="Inscrivez ici votre email" required>
            <label for="message">Votre message</label>
            <textarea name="message"  placeholder="Inscrivez ici votre message" cols="30" rows="10"></textarea>
            <label for="humeur">Votre humeur</label>
            <input type="range" name="humeur">
            <div class="boutons">
                <button type="reset">Reset</button>
                <button type="submit">Send</button>
            </div>
        </form>
    </body>
</html>
```

# Wordpress

### Introduction et installation

* .org, .com, wix, etc.
* Principe \(Bases de données, PHP, MySQL..\)
* **Installation** + One click install
* Extensions, thèmes
* Le HTML et CSS dans Wordpress

### Liens utiles

* Télécharger Wordpress
* Thèmes
* Architecture d'un thème Wordpress

### Installation de WP: les étapes

* Télécharger WP \(.org !\), en français par ex.
* Créer une base de données \(notez les user et password + serveur de BDD\)
* Créer un dossier wp à la racine de votre serveur, par exemple.
* Dézippez l'archive WP que vous avez téléchargée
* Glissez son contenu dans le dossier wp que vous venez de créer sur votre serveur
* Ensuite, accédez à monsite.com/wp dans votre navigateur préféré
* Configurez
* Modifiez le background de la homepage