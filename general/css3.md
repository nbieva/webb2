---
title: Templates (suite)
lang: fr-FR
---

# Au programme aujourd'hui

Choisissez un template HTML/CSS/JS à partir duquel construire votre propre site. Vous trouverez régulièrement des sites construit avec Bootstrap.

+ Maquettes: chercher la structure sous-jacente
+ Préparer ses images: [https://tinypng.com/](https://tinypng.com/), [https://compressor.io/](https://compressor.io/), [https://imagecompressor.com/](https://imagecompressor.com/)
+ Templates HTML (suite)
+ [https://graphiste.com/blog/templates-html-css](https://graphiste.com/blog/templates-html-css), [https://templated.co/](https://templated.co/), [https://html5up.net/](https://html5up.net/) 
+ Website builders
+ Background image

+ **gradients + générateurs CSS**
+ cursor
+ animation
+ ...

<!-- # Pour vous aider avec les positionnements CSS

* [http://flexboxfroggy.com/\#fr](http://flexboxfroggy.com/#fr) \(Jusqu'au \#13 vous aidera déjà énormément!\)
* [http://fr.learnlayout.com/](http://fr.learnlayout.com/) -->

**Voici un petit résumé, ou mémo, sur ce qui a été vu jusque maintenant:**

+ Créer une **page simple HTML** avec quelques éléments de base (div, span, titres, paragraphes, listes à puces, images, etc..)
+ Comprendre la syntaxe et la structure d'un document HTML
+ Savoir créer l'**architecture d'un site** (organisation des fichiers et dossiers)sur son ordinateur par exemple.
+ Savoir naviguer dans cette architecture (remonter d'un dossier, adresses des **liens, relatifs et absolus**, etc..)
+ Savoir mettre en ligne, via **FTP**, ce contenu sur un serveur (souscrire à un hébergement, configurer FileZilla, transférer les fichiers..)
+ Lier une **feuille de style CSS** externe à un fichier HTML.
+ Comprendre la syntaxe d'une règle CSS
+ Comprendre ce que sont les **classes** (attributs HTML).
+ Savoir les utiliser dans CSS (savoir comment cibler des éléments de votre fichier html à l'aide de CSS)
+ Comprendre les types **BLOCK** et **INLINE** d'éléments HTML
+ Comprendre comment les modifier via la propriété css **DISPLAY** (savoir ce que chaque valeur de la propriété implique.. Celles vues au cours du moins.)
+ Créer un menu de navigation horizontal.
+ Savoir utiliser les sélecteurs CSS repris dans la liste ci-dessous.
+ Donner une image d'arrière plan à une page web
+ Charger une (ou plusieurs) nouvelles polices de caractères pour votre site web via **Google Fonts**, et les utiliser dans vos CSS.
+ Utiliser un template statique HTML/CSS
+ Aligner le contenu d'une page ou d'un élément à l'aide de Flexbox, comme nous l'aurions fait avec la fenêtre alignements dans Indesign..
+ Comprendre, de façon très basique, le fonctionnement d'une bibliothèque javascript de type [Fancybox](https://fancyapps.com/fancybox/3/)
+ Savoir formater et dimensionner ses images pour un usage sur le web.
+ Savoir, bien entendu, mettre son site à jour

**Liste de propriété CSS utiles, abordées au cours (faites une recherche sur le web si nécessaire...). En vrac:**

+ display
+ width
+ height
+ padding
+ margin
+ border
+ background
+ color
+ border-radius
+ text-decoration
+ text-transform
+ opacity
+ les différentes unités (px, vw, vh, %, ..)
+ ...



---



