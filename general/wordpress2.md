---
title: Wordpress + autres CMS et frameworks
lang: fr-FR
---

# Wordpress

### Introduction et installation

* .org, .com, wix, etc.
* Principe \(Bases de données, PHP, MySQL..\)
* **Installation** + One click install
* Extensions, thèmes
* Le HTML et CSS dans Wordpress

### Liens utiles

* Télécharger Wordpress
* Thèmes
* Architecture d'un thème Wordpress

### Installation de WP: les étapes

* Télécharger WP \(.org !\), en français par ex.
* Créer une base de données \(notez les user et password + serveur de BDD\)
* Créer un dossier wp à la racine de votre serveur, par exemple.
* Dézippez l'archive WP que vous avez téléchargée
* Glissez son contenu dans le dossier wp que vous venez de créer sur votre serveur
* Ensuite, accédez à monsite.com/wp dans votre navigateur préféré
* Configurez
* Modifiez le background de la homepage

### Dans Wordpress

* Types de contenus \(articles, pages, medias..\) et Custom posts types
* Menus
* Extensions
* Widgets
* Thèmes
+ Installer un thème > [https://fr-be.wordpress.org/themes/onepress/](https://fr-be.wordpress.org/themes/onepress/)
+ Où se situent les thèmes?
+ Architecture de Wordpress sur le serveur

### Ce qui est spécifique aux thèmes
+ Les **emplacements de menus**
+ Les **types de contenus** supplémentaires
+ Vérifier le **menu** admin + les options et réglages.
+ A vérifier avant d'installer: Dernière mise à jour, popularité, cotes et commentaires.

### Les extensions (plugins)
+ A vérifier avant d'installer: Dernière mise à jour, popularité, cotes et commentaires.
+ GUTENBERG

### Ce que nous avons fait:
+ Téléchargement et installation de Wordpress sur votre serveur
+ Articles(+ catégories), pages et medias
+ Menus, widgets

### Cette semaine:
+ Retour sur les articles, pages et catégories
+ Images à la une dans les articles
+ Thèmes
+ Customisation CSS (ou html)
+ Les extensions
+ Créer un thème Wordpress??

### Au cours:
+ Site en ligne
+ **Télécharger et installer le thème [OnePress](https://fr-be.wordpress.org/themes/onepress/)**
+ Modifier l'aspect des boutons (+ survol)
+ Masquer les commentaires à l'aide de CSS
+ Ajouter un titre au dessus de la sidebar (Sidebar)
+ Installer l'extension Gutenberg + tester
+ **Créer 5 articles, 2 catégories et une page**
+ **Créer un menu de navigation**
+ **Ajouter minimum quatre éléments de menu, dont un pointe vers l'extérieur (Google.com par ex.), un vers votre page et un autre vers une de vos catégories**
+ Ajouter une vidéo (Youtube, Dailymotion, Vimeo...) dans un de vos articles
+ **Ajouter/personnaliser 3 widgets**
+ Chercher un autre thème
+ **Me communiquer l'URL de votre site
**



