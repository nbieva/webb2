---
title: Javascript et P5js
lang: fr-FR
---

# javaScript et P5js

<iframe width="740" height="418" src="https://pointerpointer.com/" style="border-radius:8px;" title="RR" frameborder="0"></iframe>

[Pointer pointer](https://pointerpointer.com/) by Jonathan Puckey

## Aujourd'hui

+ Vos questions sur le travail de fin d'année **Geonetart**
+ Quelques travaux précédents
+ Vos formulaires
+ Retour rapide sur les formulaires et animations CSS
+ Material Design / **icons**
+ Les **iframes**
+ Modifier le curseur
+ Comment **developper et tester pour les mobiles**?
+ **Analyses** de sites: [RR](https://www.newrafael.com/) ci-dessus + signatures + [Alexei Shulgin](https://rhizome.org/art/artbase/artwork/form-art/) + [Michael Manning, Floating form](http://www.floatingform.net/)
+ **JavaScript**

## Javascript

Comment s'articule un script JS avec une page HTML? Où trouver de la documentation? Qu'est-ce que P5js? Comment créer un élément HTML avec javaScript? Comment manipuler des CSS avec javaScript?

+ [https://developer.mozilla.org/fr/docs/Web/JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript)
+ [jQuery](https://jquery.com/)
+ [jQuery sur Alsacréations](https://www.alsacreations.com/astuce/lire/916-librairie-javascript-jquery-script.html)
+ [P5*js](https://p5js.org/)
+ [L'éditeur de P5js en ligne](https://editor.p5js.org/)
+ [Le support en ligne du workshop B1 avec P5js](https://codep5.netlify.app/)

## Websites

+ [https://brutalistwebsites.com/](https://brutalistwebsites.com/)
+ [Désordre - Philippe De Jonckheere](https://www.desordre.net/)
+ [Alexei Shulgin](http://variants.artbase.rhizome.org/Q1249/) + Le travail sur [Rhizome.org](https://rhizome.org/art/artbase/artwork/form-art/)
+ [Michael Manning, Floating form](http://www.floatingform.net/)
+ [Patternator](https://patternator.netlify.app/)

### Intégration d'un script dans la page HTML

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Intégration de P5js</title>
        <script src="https://cdn.jsdelivr.net/npm/p5@1.3.1/lib/p5.js"></script>
        <script src="sketch.js"></script>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        ceci est le body
    </body>
</html>
```

Le script qui peut être, par exemple:

```js
function setup() {
  createCanvas(400, 400);
}

function draw() {
  if (mouseIsPressed) {
    fill(0);
  } else {
    fill(255);
  }
  ellipse(mouseX, mouseY, 80, 80);
}
```

### Media Queries

+ [Media queries](https://developer.mozilla.org/fr/docs/Web/CSS/Media_Queries/Using_media_queries) + Tester dans le navigateur pour les mobiles
+ Quelques règles de base ([Un exemple ici](https://www.alsacreations.com/astuce/lire/1177-Une-feuille-de-styles-de-base-pour-le-Web-mobile.html))
+ **media queries**: [Media Queries sur le MDN](https://developer.mozilla.org/fr/docs/Web/CSS/Media_Queries/Using_media_queries#am%C3%A9liorations_syntaxiques_avec_la_sp%C3%A9cification_de_niveau_4)

## Peut-être...

+ [https://fr.qr-code-generator.com/](https://fr.qr-code-generator.com/)
+ **Bootstrap**: [https://getbootstrap.com/](https://getbootstrap.com/)
+ Voir chez[Bootstrap](https://getbootstrap.com/docs/4.4/components/alerts/) aussi...

## Le design et les maquettes de sites

+ [Figma](https://www.figma.com/)

## Pour rappel, lors de la première lecture du travail :

* Le lieu de référence doit être choisi
* La façon doit le site en ligne fera référence ou dialoguera avec le lieu doit déjà avoir été envisagée.
* Un projet de conception de site doit être réalisé \(même si c'est sur une feuille de papier\)
* Ces différents point seront évalués.
* Soyez là au moins 5 minutes avant l'heure prévue

## Quelques travaux précédents

+ Yaël : [https://mondevirtuel.netlify.app/](https://mondevirtuel.netlify.app/)
+ Axelle: [https://quarantinebelike.netlify.app/](https://quarantinebelike.netlify.app/)
+ Eloïse: [https://eloricchiutocorrectiontravailfinal.netlify.app/](https://eloricchiutocorrectiontravailfinal.netlify.app/)
+ Célia: [https://admettredesalignerbonne.netlify.app/](https://admettredesalignerbonne.netlify.app/)
+ Alexandra: [https://acquerir-interieure-raison.netlify.app/](https://acquerir-interieure-raison.netlify.app/)
+ Nicolas: [https://baunicolas-aib2-kangdingsichuan.netlify.app/](https://baunicolas-aib2-kangdingsichuan.netlify.app/)
+ Barbara: [https://lestempsmodernes.netlify.app/](https://lestempsmodernes.netlify.app/)
+ Léa: [https://suspicious-khorana-ea29e0.netlify.app/](https://suspicious-khorana-ea29e0.netlify.app/)
+ David: [https://toponymie.netlify.app/](https://toponymie.netlify.app/)
+ Lou: [https://toutlemondeestla.netlify.app/](https://toutlemondeestla.netlify.app/)