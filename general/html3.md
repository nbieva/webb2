---
title: Inline, Block, CSS
lang: fr-FR
---

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/nFiMtHciR4Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Paul Slocum**, [Time lapse Homepage](https://artbase.rhizome.org/wiki/Q4171) (2005), video, 55'

[Time lapse Homepage](https://artbase.rhizome.org/wiki/Q4171) is a video the artist made sequentially, frame-by-frame, by editing the HTML on his homepage and taking a screenshot after each change. The soundtrack was created by compiling brief clips of audio recorded from the MIDI files playing when each screenshot was taken.

+ Evan Roth, [All HTML Elements](http://all-html.net/), 2011
+ [https://christoph-knoth.com/](https://christoph-knoth.com/)
+ **Archiver le web.** [Internet Archive](https://archive.org/), [Wayback machine](https://web.archive.org/), [Webrecorder](https://webrecorder.net/)

### Nous savons:

* Ce qu'est **HTML**
* Ce que sont **navigateur**, **serveur**, **IP**, **nom de domaine**..
* Ce qu'est un **inspecteur web** et ce qu'est un code source.
* Créer une **page HTML simple**
* Ce que sont les **balises** `<head>`, `<title>`, `<meta>`, `<body>` ainsi que toute une série de balises du corps de la page.. \(`<h1>`, `<h2>`, `<p>`, `<ul>`, `<li>`, `<a>`, `<img>`, etc. \)
* Utiliser un **éditeur de code** et faire tourner un **serveur web** sur notre machine grâce à Live Server
* Créer plusieurs pages entre elles, interconnectées, avec un **menu de navigation**.
* **Mettre en ligne** un projet sur le web, et le mettre à jour.
* Un peu de ce qu'il en est des **URL de fichiers** \(Chemins relatifs et absolus\)
* **Recherche d'images** et copies d'adresses \(URLs\)

### Aujourd'hui

+ **Liens** vers une adresse email? (mailto: ) Vers un numéro de téléphone? (tel: )
+ Les **balises** `<div>` et `<span>` (+ button, etc..)
+ Les éléments de type **inline**, **block**
+ [**CSS Zen Garden**](http://www.csszengarden.com/)
+ Lier une feuille de styles **CSS** à un fichier HTML
+ La propriété CSS [**background**](https://developer.mozilla.org/fr/docs/Web/CSS/background)
+ Les **images**: Résolution, tailles, formats pour le web
+ [Google WebFonts](https://fonts.google.com/): aussi pour votre ordinateur (Comment charger une police)

------

# Liens relatifs et absolus

Il existe 2 principales manière d'indiquer le chemin d'accès à un fichier \(HTML,img, PDF..\) à partir d'un autre. On parlera de chemins relatifs, ou absolu.

* [Liens, Chemins relatifs et absolus](https://www.w3schools.com/html/html_filepaths.asp). Ancres à l'intérieur d'un document.

### Le chemin relatif

Le chemin relatif, l'est par rapport à cotre page HTML, par exemple. On indiquera le chemin A PARTIR DE votre fichier. Si la relation entre les deux change, le fichier sera "perdu".

```html
<a href="dossier/page.html">Ma page</a>
```
ou si on veut atteindre un fichier, dans un dossier placé à côté de notre page :
```html
<a href="../dossier/page.html">Ma page</a>
```
Le chemin suivant sera valide, même si vous déplacez votre page. Il référence le fichier à partir de la racine du serveur. (slash au début)
```html
<a href="/dossier/page.html">Cliquez ici</a>
```


### Le chemin absolu

Le chemin absolu sera également valide, même si vous déplacez votre page.

Si vous faites un "clic-droit" + "Copier l'adresse de l'image" sur une image dans votre navigateur. Vous obtiendrez une URL de ce genre:

```html
<a href="https://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Visual_Principal_340/4/1/4/9782070404414/tsp20121014065225/Le-chercheur-d-absolu.jpg">Cliquez ici</a>
```
Un chemin absolu peut parfois être bien plus simple que celui ci-dessus.

#### MAIS!
Dans tous les cas, si un fichier ou un dossier est renommé, ou si une extension change \(même de .jpg à .JPG\), le chemin sera mort et le fichier non-trouvé.

![](/assets/liensabsolusetrelatifs.png)

# Ancres

```html
<a href="#mon_ancre">Aller à mon ancre</a>
    <div id="mon_ancre">
</div>
```

# CSS

Vous trouverez ci-dessous le sélecteur universel définissant le modèle de boîte à utiliser pour tous vos éléments (à copier-coller dans le haut de votre feuille de styles).

```css
* {
box-sizing: border-box;
}
```

## Propriétés
+ color
+ background
+ padding
+ margin
+ border
+ Block & inline
+ ... (voir [cette liste](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3/memento-des-proprietes-css) ou [cette page](https://www.supinfo.com/articles/single/3865-principales-proprietes-langage-css-leurs-valeurs).



## Mais aussi...

+ Hacker les CSS d'un site en ligne via l'inspecteur
+ Styler un menu de navigation
+ Insertion d'une video Youtube/vimeo ou d'une map

Eventuellement [transfert FTP](/transfert-ftp.md) test@ testartsnum2018;
et Eléments de [formulaires](https://www.w3schools.com/html/html_form_elements.asp) : form, input, textarea, label, select, option, datalist