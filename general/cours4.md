---
title: Introduction à CSS
lang: fr-FR
---

# Introduction à CSS

![](/assets/francine.png)

+ [http://diana-adrianne.com/purecss-francine/](http://diana-adrianne.com/purecss-francine/)
+ [https://github.com/cyanharlow/purecss-francine](https://github.com/cyanharlow/purecss-francine)
+ [Francine dans le temps...](https://www.vox.com/2018/5/3/17309078/digital-art-diana-a-smith-francine-coded-browser-art)
+ [https://bengrosser.com/projects/safebook](https://bengrosser.com/projects/safebook)


#### Premiers éléments de mise en page

+ [Francine dans le temps...](https://www.vox.com/2018/5/3/17309078/digital-art-diana-a-smith-francine-coded-browser-art)
+ Les feuilles de styles en cascade (Feuilles de styles externes et styles inline)
+ Propriétés CSS: [Document partagé](https://pads.domainepublic.net/p/css)
+ Premières déclarations CSS
+ La propriété **display** (Inline, block, flex, none)
+ Couleurs (text, background, border)
+ **:hover**

Vous pouvez faire vos tests dans [https://jsbin.com](https://jsbin.com) par exemple, avant de les rapatrier sur votre machine...


## Comment cibler un (ou des) élément(s) avec CSS

```css
/* On cible les paragraphes (P) */
p {
color:#333;
}

/* On cible les éléments SPAN qui sont dans un paragraphe */
p span {
color:red;
}

/* On cible les paragraphes avec la classe INTRODUCTION */
p.introduction {
color:blue;
}

/* On cible les éléments (quels qu'ils soient) avec la classe INTRODUCTION */
.introduction {
/* propriétés */
}
/* On cible les éléments SPAN qui sont dans un élément(quel qu'il soit) avec la classe INTRODUCTION */

.introduction span {
/* propriétés */
}

/* On cible les liens qui sont dans des LI, qui sont dans des UL (c'est généralement le cas) */
ul li a {
/* propriétés */
}

/* On cible les liens survolés qui sont dans des LI, qui sont dans des UL */
ul li a:hover {
/* propriétés */
}

img {
opacity:0.9;
}

img:hover {
opacity:1;
}

.legende {
display:none;
}

```

## Propriétés CSS utiles

+ cursor
+ interactivité 
+ background, border, background-image, 
+ ... (voir [cette liste](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3/memento-des-proprietes-css) ou [cette page](https://www.supinfo.com/articles/single/3865-principales-proprietes-langage-css-leurs-valeurs).
+ Mise en forme du texte (**font-family**, **font-size**, **color**, **text-decoration**)
+ Pseudo classe **:hover**
+ Background
+ Background image (diff sur chaque page)
+ Gradients

### Color

+ color (pour le texte)
+ background (ou background-color)
+ linear-gradient : [http://www.colorzilla.com/gradient-editor/](http://www.colorzilla.com/gradient-editor/)

### Typographie

+ font-family
+ font-size
+ font-weight
+ font-style
+ text-decoration
+ text-transform
+ text-align
+ line-height
+ color

### Background

+ background
+ background-color
+ background-size
+ background-repeat
+ background-position

### Position

+ position
+ float
+ (flex)
+ z-index

### Animation

+ [transitions](https://daneden.github.io/animate.css/)

## Liens:

+ [https://www.pixelcrea.com/ressources/memo-css3.pdf](https://www.pixelcrea.com/ressources/memo-css3.pdf)
+ [https://jenseign.com/html/wp-content/uploads/2018/01/memo-html-apprendre.pdf](https://jenseign.com/html/wp-content/uploads/2018/01/memo-html-apprendre.pdf)
+ CSS Zen Garden: [http://www.csszengarden.com/](http://www.csszengarden.com/)
