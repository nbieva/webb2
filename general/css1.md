---
title: Templates HTML
lang: fr-FR
---

![](/assets/albers2.png)

## Ce que nous avons vu

+ Structure d'un **document HTML**
+ Balises courantes
+ Mise en ligne sur **Netlify**
+ **Architecture** d'un projet web ou d'un site
+ **Chemins** relatifs et absolus
+ Création de plusieurs **pages connectées** entre elles
+ **[Feuilles de styles, règles CSS de base](http://curlybraces.be/wiki/Ressources::CSS)**
+ Les **classes** (attribut HTML)

## Aujourd'hui

+ Retour sur + [CSS Zen Garden](http://www.csszengarden.com/)
+ Vos questions et/ou besoins
+ Le **modèle de boîte** CSS (margin, padding, border)
+ Ajouter une police: [Google fonts](https://fonts.google.com/)
+ [Gradients](https://cssgradient.io/) + [Gradients](https://cssgradient.io/gradient-backgrounds/)
+ [Shadows](https://www.cssmatic.com/box-shadow)
+ CSS minimum pour un **mobile**
+ [HTML template](https://html5up.net/) / + [Lens](https://html5up.net/lens) ou [Paradigm Shift](https://html5up.net/paradigm-shift)
+ Qu'est-ce que Javascript + la **balise script**

## Template HTML-CSS (aussi pour la semaine prochaine)

![](/assets/html-template.png)

Sur base de ce qui a été vu au cours, téléchargez, modifiez et mettez en ligne sur Netlify le template HTML suivant : [https://html5up.net/lens](https://html5up.net/lens) (téléchargez en haut à droite).

Vous pouvez en choisir [un autre](https://html5up.net/) si vous le désirez mais [celui proposé](https://html5up.net/lens) est relativement simple à prendre en main.

+ **Remplacez les images** par les vôtres (petits formats et grands formats) et modifiez leurs légendes et descriptions.
+ Modifiez les liens des **réseaux sociaux**.
+ Modifiez le **titre** et la **description** de la page et tout autre élément HTML que vous jugeriez nécessaire.
+ Ajoutez au moins une **[police de caractère](https://fonts.google.com/)** supplémentaire.
+ Insérez un **lien vers un PDF** (téléchargement)
+ Testez.
+ Mettez le tout en ligne sur **[Netlify](https://www.netlify.com/)** (Ce sera donc votre quatrième site dans l'interface de Netlify). Les précédents doivent rester accessibles. Pour rappel, ce que vous glissez dans la fenêtre de Netlify doit toujours **être un dossier** et **un fichier nommé index.html doit toujours être présent à la racine** de ce dossier.
+ Testez **sur un téléphone ou une tablette**.
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1Oo_rcx9GV6DQPqHUWTVcZqyZoQdw4EQsiwldJ14oJGo/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice.

::: tip
Classes: custom-block + tip
:::

::: warning
Classes: custom-block + warning
:::

::: danger
Classes: custom-block + danger
:::
