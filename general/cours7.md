---
title: AlberCSS
lang: fr-FR
---

# Flexbox en pratique

Réalisation d'un tableau de Josef Albers (ou autre similaire) en utilisant les propriété CSS vues jusqu'à présent, les différentes unités, les interactions de base, mais surtout les différentes propriétés liées au modèle de boîte flexible (flexbox) que nous avons vues avec [Flexbox Froggy](https://flexboxfroggy.com/#fr).

L'exercice commencé au cours, nous permet de revenir ensemble sur les différentes notions vues, de les clarifier et les mettre en pratique.

------ 

# Hommage à l'hommage² (AlberCSS)

A partir du code HTML repris ci-dessous, créez votre propre hommage à l'hommage au carré.
Utilisez pour ceci:

+ Flexbox (pour les alignements)
+ width
+ height
+ padding
+ margin
+ border
+ background (images, [dégradés](https://cssgradient.io/) et autres...)
+ unités relative à la taille de votre fenêtre, vh et vw
+ et bien sûr les effets de survol
+ et pourquoi pas les transitions ou [animations CSS](https://animista.net/)
+ Mettez le tout en ligne sur **[Netlify](https://www.netlify.com/)** Pour rappel, ce que vous glissez dans la fenêtre de Netlify doit toujours **être un dossier** et **un fichier nommé index.html doit toujours être présent à la racine** de ce dossier.
+ Testez **sur un téléphone ou une tablette**. 
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice.

La vidéo ci-dessous pourra en aider certain.e.s

<iframe width="740" height="460" src="https://www.youtube.com/embed/fV0CawAjuo0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Hommage au carré</title>
        <link rel="stylesheet" media="screen" href=""><!-- Ajoutez ici le chemin d'accès vers votre feuille de styles évidemment.. -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Un hommage au carré, basé sur le travail de Josef Albers, utilisant CSS, avec un marquage HTML minimal.">
    </head>
    <body>
        <section>
            <div>
                <div>
                </div>
            </div>
        </section>
    </body>
</html>
```

![](https://github.com/lam-artsnum/b2/wiki/images/albers.png)
![](https://crennjulie.files.wordpress.com/2012/03/albers-josef-21.jpg)
![](https://medias.gazette-drouot.com/prod/medias/mediatheque/43761.jpg)
![](https://images.copyrightbookshop.be/site/wp-content/uploads/2019/01/13155446/52006.jpg)
![](https://madd-bordeaux.fr/sites/madd/files/styles/lightbox/public/2020-08/sliders/92173737_3385754648104665_6791967733211725824_n.jpg?itok=E3Yt2_pL)