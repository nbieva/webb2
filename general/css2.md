---
title: Flexbox
lang: fr-FR
---

![](/assets/albers2.png)

Nous revenons aujourd'hui sur les Templates HTML et les différentes bases desquelles vous pouvez partir pour  créer votre projet. Nous reviendrons également sur le modèle de boîte Flex.

Nous avions vu:

+ Mise en page Flexbox: un exemple
+ Les [Media-Queries](https://www.alsacreations.com/article/lire/930-css3-media-queries.html)
+ Bibliothèques javascript additionelles
+ Templates statiques
+ [Fancybox](https://fancyapps.com/fancybox/3/)
+ [Animations](https://animista.net/) CSS
+ Background images

Pour semaine prochaine:

+ Faire une maquette
  
## Aujourd'hui

+ Retour sur CSS et les templates HTML
+ Le [modèle de boîte](https://learn.shayhowe.com/html-css/opening-the-box-model/) (box-sizing:border-box;)
+ Les unités (px, %, em, rem, vh, vw, cm..)
+ Bibliothèques javascript additionelles (ex fancybox)
+ Mise en page  Flexbox
+ Les [Media-Queries](https://developer.mozilla.org/fr/docs/Web/CSS/Media_Queries/Using_media_queries)

```css
body {
    background:green;
}

@media (max-width: 768px) {
    body {
        background:yellow;
    }
}
```

## Exercice Albers

Voir exercices

## Les propriétés CSS du jour

+ display
+ justify-content
+ align-items
+ width
+ height
+ padding (ou padding-top, padding-right, etc… )
+ margin (ou margin-top, margin-right, etc… )
+ border (ou border-top, border-right, etc… )
+ :hover
+ border-radius
+ background
+ background-image
+ background-repeat
+ [background-size](https://css-tricks.com/almanac/properties/b/background-size/)
+ background-position
+ backgroud-color
+ [video background](https://www.youtube.com/watch?v=emL9dkijfZY)
+ font-size
+ font-weight
+ font-family

![](https://camo.githubusercontent.com/7a68ce3b3f3e4552739934c6978874139d7ff4ff/68747470733a2f2f6373732d747269636b732e636f6d2f77702d636f6e74656e742f636c617373706c757369642e706e67)

![](/assets/block.png)

![](https://github.com/lam-artsnum/b2/wiki/images/albers.png)

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Mise en page Flexbox</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Mise en page Flexbox">
        <link rel="stylesheet" media="screen" href="flex.css">
    </head>
    <body>
        <header>
            <div class="container">
                <h1>Mise en page avec Flexbox</h1>
            </div>
        </header>
        <div class="container content">
            <nav>
              <a href="apropos.html">A propos</a>
              <a href="travaux.html">Travaux</a>
              <a href="agenda.html">Agenda</a>
              <a href="contact.html">Contact</a>
            </nav>
            <main>
                <h2>Mes articles</h2>
                <article>
                    <h3>Lorem ipsum</h3>
                    <p>
                      Aliquam non euismod enim, iaculis pretium massa. Aenean eu convallis mi. Nunc finibus nunc at placerat semper. Nunc aliquam erat nec tincidunt fermentum. Sed dictum turpis et ligula suscipit tincidunt. Nunc efficitur sit amet felis non condimentum. Proin fringilla nisl sed dictum efficitur. Proin tempus nisi lorem, eu porttitor dui scelerisque non.
                    </p>
                </article>
                <article>
                    <h3>Vestibulum ante ipsum</h3>
                    <p>
                        <a href="https://fr.wikipedia.org/wiki/Portrait_de_Bindo_Altoviti" target="_blank">
                          <img src="bindo-altoviti.jpg" alt="Bindo Altoviti, Raphael" title="Bindo Altoviti, Raphael">
                        </a>
                        Maecenas eleifend feugiat odio at vehicula. Nam sed mauris at dolor porta ultrices. Integer auctor iaculis metus ac congue. Nullam nec suscipit mauris. Pellentesque ultrices, mauris sed tincidunt placerat, tortor purus commodo ipsum, quis posuere sapien diam sed odio. Nam in metus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent tempor lorem sed tristique imperdiet. Pellentesque ut diam egestas, accumsan dolor eget, egestas libero. Quisque lacinia neque ac eros malesuada sagittis. Vestibulum lacinia, mi sed tristique accumsan, lacus tortor auctor nulla, ut maximus quam purus non erat.
                    </p>
                    <p>
                        Maecenas eleifend feugiat odio at vehicula. Nam sed mauris at dolor porta ultrices. Integer auctor iaculis metus ac congue. Nullam nec suscipit mauris. Pellentesque ultrices, mauris sed tincidunt placerat, tortor purus commodo ipsum, quis posuere sapien diam sed odio. Nam in metus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent tempor lorem sed tristique imperdiet. Pellentesque ut diam egestas, accumsan dolor eget, egestas libero. Quisque lacinia neque ac eros malesuada sagittis. Vestibulum lacinia, mi sed tristique accumsan, lacus tortor auctor nulla, ut maximus quam purus non erat.
                    </p>
                </article>
            </main>
        </div>
        <footer>
            <p>Source: <a href="http://fr.lipsum.com/" target="_blank">Lorem ipsum</a></p>
        </footer>
    </body>
</html>
```

## Ressources utiles:

+ Sur les bases de CSS: [http://curlybraces.be/wiki/Ressources::CSS](http://curlybraces.be/wiki/Ressources::CSS)
+ [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
+ [Flexbox froggy](https://flexboxfroggy.com/)
+ [https://www.youtube.com/watch?v=fV0CawAjuo0&feature=emb_imp_woyt](https://www.youtube.com/watch?v=fV0CawAjuo0&feature=emb_imp_woyt)
+ [http://www.digitalab.be/web-b2-02-fancybox-n-bieva/](http://www.digitalab.be/web-b2-02-fancybox-n-bieva/)