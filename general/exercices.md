<div class="notification success">
    <p>Conservez toujours une <strong>sauvegarde</strong> de tous vos travaux et exercices sur votre ordinateur ou un support externe (clé USB, disque..)</p>
</div>

# Ajout de contenu dans Wordpress <div class="badges"><span class="remise">Pour le 16 et 17 mai </span></div>

Après avoir installé Wordpress (ce qui devrait être fait), créez, dans l'interface d'administrationd e votre site les éléments suivants:

+ Créez au moins 3 catégories pour classer vos articles.
+ Ajoutez au moins 5 articles (avrec titre, contenu et "image à la une")
+ Classez-les dans la (ou les) catégories voulues.
+ Testez
+ Vérifiez dans notre fichier habituel que l'url de votre site Wordpress est bien mentionnée.


# Mise en ligne d'une galerie photo <div class="badges"><span class="remise">Au cours mais à rendre</span></div>

Sur base de ce qui a été vu au cours, téléchargez, modifiez et mettez en ligne sur Netlify le template HTML suivant : https://html5up.net/lens (téléchargez en haut à droite)

+ Remplacez les images par les vôtres (petits formats et grands formats) et modifiez leurs légendes et descriptions.
+ Modifiez également le titre et la description de la page et tout autre élément HTML que vous jugeriez nécessaire.
+ Mettez en ligne sur Netlify
+ Copiez l'URL dans notre fichier habituel.

Et, évidemment, mettez-vous en ordre pour l'exercice précédent (installation de Wordpress, voir les vidéos ci-dessous). ceci est indispensable pour notre dernier cours.


# Installation de Wordpress <div class="badges"><span class="remise">Pour les 9 et 10 mai</span></div>

Je vous ai mis en ligne [quelques videos, réunies en une playlist](https://www.youtube.com/playlist?list=PLDdoBMCKjS0a_UfMfmmcQ7qELUpl76w-T). Elles reprennent depuis le début ces derniers aspects plus techniques: Compte webhost + FileZilla + Transfert de fichiers + Installation de Wordpress.

Pour la semaine prochaine, il vous faut donc installer Wordpress sur un de vos sites chez Webhost (Vidéo 3), manuellement ou "automatiquement"(video 4) si la publicité ne vous dérange pas...

Notez l'adresse de votre site Wordpress dans [notre fichier habituel](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing).

<iframe width="770" height="430" style="margin-top:1.5rem;" src="https://www.youtube.com/embed/videoseries?list=PLDdoBMCKjS0a_UfMfmmcQ7qELUpl76w-T" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


# Pour la rentrée <div class="badges"><span class="remise">25 et 26 avril</span></div>

+ Inscription sur [000Webhost](https://fr.000webhost.com/) (compte gratuit!)
+ Télécharger [Filezilla](https://filezilla-project.org/) (Client)
+ Se connecter au serveur et déposer à la racine de celui-ci (dans le répertoire public_html) un fichier index.html avec un contenu de votre choix.
+ Noter l'URL (l'adresse de votre page) dans [notre fichier habituel](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing).

# Avant le congé <div class="badges"><span class="remise">4 et 5 avril</span></div>

+ Veuillez à vous mettre en ordre pour tous les derniers exercices. (testez-les, please).

# Bootstrap <div class="badges"><span class="remise">28 mars (cours du jeudi)</span></div>

+ Créer une page web utilisant Bootstrap ( https://getbootstrap.com/ )
+ Utilisez minimum 4 composants de Bootstrap (navbar, spinners, list group, buttons, etc...)
+ Mettez en ligne sur Netlify et postez l'adresse dans la colonne adéquate, à hauteur de votre nom, dans [ce fichier](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing).

# AlberCSS (ou BauhauCSS) <div class="badges"><span class="remise">29 mars (cours du vendredi)</span></div>

+ Créez votre propre hommage au carré \(Full CSS évidemment!\)sur base du code HTML que vous trouverez dans la barre latérale \("Albers"\). Notez que vous pouvez modifier ce code si vous désirez réaliser un autre tableau d'Albers (ou une image de quelqu'un d'autre).
+ Liez une feuille de style au fichier HTML
+ Stylez à l'aide de Flexbox, des marges, de width et height, backgrounds, etc..
+ Ajoutez de l'interactivité \(effets de survols, liens externes, animations, images..\). Soyez inventifs.
+ Mettez en ligne sur Netlify et postez l'adresse dans la colonne adéquate, à hauteur de votre nom, dans [ce fichier](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing).

--------

# Flexbox <div class="badges"><span class="remise">7 et 8 mars</span></div>

+ Suivez et réalisez les **13 premières étapes** de ce tutoriel sur Flexbox > [https://flexboxfroggy.com/#fr](https://flexboxfroggy.com/#fr)

Flexbox reprend un ensemble de propriétés CSS qui nous seront **extrêmement utiles pour nos mises en pages et nos alignements** avec CSS. Il est extrêmement important d'intégrer ces quelques étapes avant le prochain cours.

# CSS Zen Garden <div class="badges"><span class="remise">28 février et 1er mars</span></div>

+ Récupérez le code de [CSS Zen Garden](http://www.csszengarden.com/) (code dans la colonne de gauche) et enregistrez votre nouveau document HTML
+ Créez une feuille de style CSS externe.
+ Liez ce document HTML à cette feuille de style, présente dans un dossier "css" à côté de votre fichier HTML, comme nous l'avons fait au cours.
+ Modifiez son apparence **en stylant au moins 5 éléments** de votre choix
+ Faites quelques essais en utilisant éventuellement les classes déjà présentes dans ce document (ou créez vos propres classes)
+ TESTEZ!

##Remise:

L'URL de votre travail doit être collée dans la colonne adéquate, à hauteur de votre nom, dans [ce fichier](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing). Pour ceux qui sont concernés, un dépôt exceptionnel dans votre dossier Dropbox est permis, avant la date du prochain cours. 
Les solutions suivantes sont cependant préférables:

+ Utilisation de **Netlify** pour la mise en ligne. Nous l'avons retesté à La Cambre et cela a fonctionné dans la grande majorité des cas. Et il n'y a **aucune** raison que cela ne fonctionne pas de l'extérieur.
+ Ou: **utilisation d'outils tels [jsBin](https://jsbin.com) ou [CodePen](https://codepen.io/)**. Il vous faudra alors créer un compte afin d'enregistrer votre travail et de pouvoir le partager (pour récupérer l'URL et la noter dans notre fichier))


--------

# Premier site web <div class="badges"><span class="remise">21 & 22 février</span></div>

Sauf indication contraire, à partir de ce 14/02, les exercices sont **systématiquement** à mettre en ligne et à rendre via [ce fichier](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing).

Créez votre **premier site web**, à savoir: 

* Un dossier comprenant une série de fichiers, organisés en répertoires, et liés entre eux (fichiers html, images ..)
* Vos pages html doivent comprendre les différents éléments évoqués au cours (p, [img](https://www.w3schools.com/tags/tag_img.asp), [ul, li](https://www.w3schools.com/tags/tag_li.asp), h1, h2, etc..)
* Présence d'un **menu de navigation**, évidemment, entre vos pages. Pour cela, utilisez des liens (a) dans des listes (ul + li)
* Testez vos pages et votre navigation
* Mettez votre site en ligne via Netlify si vous avez pu le mettre en place et copiez l'adresse dans [ce fichier](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing). 
Dans le cas contraire, mettez votre dossier racine dans votre dossier dropbox.

Attention à la syntaxe et la structure de vos pages html.

##Remise:

L'URL de votre travail doit être collée dans la colonne adéquate, à hauteur de votre nom, dans [ce fichier](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing). Pour ceux qui sont concernés, un dépôt exceptionnel dans votre dossier Dropbox est permis, avant la date du prochain cours. 
Les solutions suivantes sont cependant préférables:

+ Utilisation de **Netlify** pour la mise en ligne. Nous l'avons retesté à La Cambre et cela a fonctionné dans la grande majorité des cas. Et il n'y a **aucune** raison que cela ne fonctionne pas de l'extérieur.
+ Ou: **utilisation d'outils tels [jsBin](https://jsbin.com) ou [CodePen](https://codepen.io/)**. Il vous faudra alors créer un compte afin d'enregistrer votre travail et de pouvoir le partager (pour récupérer l'URL et la noter dans notre fichier)


--------

# Page web + Netlify <div class="badges"><span class="remise">14 & 15 février</span></div>

##1. Page web

Créez une page web avec les éléments repris ci-dessous. Vous pouvez pour cela partir de la base que vous trouverez dans le menu de gauche (sous Codes \> Ultra basic html ). Apportez votre dossier (avec votre page et vos images) au prochain cours. 
Nous regarderons les résultats ensemble en début de cours.
Eléments à intégrer:

* Une structure correcte \(head, body..\)
* Modifiez le titre de la page \(balise "title" dans l'entête "head"\)
* Au moins 2 niveaux de titres \(h1, h2, h3, etc.\)
* Au moins 3 paragraphes
* Au moins 2 images \([Balise html img](https://www.w3schools.com/tags/tag_img.asp)\)
* Au moins une liste à puce \([Eléments ul et li](https://www.w3schools.com/tags/tag_li.asp)\)


##2. Netlify

Créez un compte sur **[Netlify](https://www.netlify.com/)**. Nous utiliserons Netlify, dans un premier temps, pour mettre nos pages en ligne. Ce sera au programme du prochain cours.
Vous pouvez également, pour ceux qui le désirent, créer un compte sur **[Code Pen](https://codepen.io)**, qui fera office de bac à sable.

--------

Vos fichiers HTML peuvent m'être envoyés [par email](mailto:nicolasbieva@gmail.com) ou apportés au cours sur clé USB (cette semaine uniquement).
A partir de ce 14/02, les exercices seront systématiquement à mettre en ligne et à rendre via [ce fichier](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing).


