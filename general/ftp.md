---
title: Transfert FTP
lang: fr-FR
---

# Transfert FTP

**Ce que nous avons à notre disposition:**

Un langage qui permet de structurer du contenu et de lier des documents entre eux \(site\) ou vers d'autres documents ou ensembles de documents distants \(d'autres sites\).

Nous allons aujourd'hui tenter de passer une étape importante: le transfert de fichiers vers un serveur, en utilisant un client FTP, à savoir FileZilla. FileZilla est stable, libre et gratuit. Vous trouverez évidemment d'autres applications de ce type, payantes ou non.

Il nous faudra pour cela mettre en place un **serveur distant** actif pour héberger nos fichiers

> Note: le serveur n'est pas à proprement parler l'ordinateur sur lequel sont stockés les fichiers, mais un programme qui tourne sur ce dernier, à l'écoute de nos requêtes éventuelles.

Pour cela, nous allons créer un compte chez un **hébergeur** \(One.com, OVH, Gandi, Hostinger...\) La [solution d'hébergement gratuite de chez Hostinger](https://www.hostinger.fr/hebergement-gratuit) peut être une bonne solution. La plupart des hébergeurs sont payants, même si le coût reste raisonnable pour un hébergement standard.

Quelques écrans détaillant la procédure sont repris ici: https://nbieva.gitbooks.io/web/content/transfert-ftp.html

Dans notre cas, nous allons souscrire à un hébergement chez **Hostinger**, un des seuls à proposer une formule gratuite.

Les informations que vous devez noter sont:

* L'**adresse de votre serveur** FTP \(genre ftp.monserveur.com\)
* Votre** nom d'utilisateur **\(username, login..\)
* Votre **mot de passe**

[Téléchargez ensuite FileZilla](https://filezilla-project.org/)

> Note: nous avons eu beaucoup de soucis l'année dernière pour les connexions FTP. Ceci est dû au fait que les serveurs de Hostinger recoivent un grand nombre de requêtes en même temps, provenant de la même adresse IP \(celle de l'école\). Les connexions sont alors bloquées. C'est une sécurité des serveurs pour prevenir les attaques en déni de service. \(DDoS\)

![](/assets/filezilla.png)

---


![](/assets/Capture d’écran 2018-02-07 à 15.29.12.png)

![](/assets/Capture d’écran 2018-02-07 à 15.29.26.png)

![](/assets/Capture d’écran 2018-02-07 à 15.30.11.png)

![](/assets/Capture d’écran 2018-02-07 à 15.30.25.png)

![](/assets/Capture d’écran 2018-02-07 à 15.30.53.png)

![](/assets/Capture d’écran 2018-02-07 à 15.31.22.png)

![](/assets/Capture d’écran 2018-02-07 à 15.31.35.png)

![](/assets/Capture d’écran 2018-02-07 à 15.34.10.png)

#Filezilla

![](/assets/Capture d’écran 2018-02-07 à 15.34.42.png)

![](/assets/Capture d’écran 2018-02-07 à 15.36.37.png)

![](/assets/Capture d’écran 2018-02-07 à 15.36.58.png)

![](/assets/Capture d’écran 2018-02-07 à 15.37.19.png)

![](/assets/Capture d’écran 2018-02-07 à 15.37.30.png)

![](/assets/Capture d’écran 2018-02-07 à 15.38.57.png)

![](/assets/Capture d’écran 2018-02-07 à 15.41.49.png)

![](/assets/Capture d’écran 2018-02-07 à 15.44.36.png)

![](/assets/Capture d’écran 2018-02-07 à 15.38.08.png)

![](/assets/Capture d’écran 2018-02-07 à 15.38.29.png)

![](/assets/Capture d’écran 2018-02-07 à 15.44.56.png)

![](/assets/Capture d’écran 2018-02-07 à 15.45.37.png)