---
title: Quelques balises
lang: fr-FR
---

[![](https://d32dm0rphc51dk.cloudfront.net/ecTqQsonsjyPGHIduxNwTg/larger.jpg)](https://www.newrafael.com/notes-on-abstract-browsing/)

**Rafael Rozendaal,** [Abstract browsing](http://www.abstractbrowsing.net/) 17 03 06\(Flickr\), 2017

Ce travail de Rafael Rozendaal, une extension pour le navigateur Chrome, nous donne un indice sur la structure de cette page HTML \(ici, Flickr\). Nous parlerons beaucoup d'**éléments** HTML, souvent de blocs, imbriqués les uns dans les autres \(**BLOCK**\), ou les uns à côté des autres \(**INLINE**\).

+ [JODI](http://wwwwwwwww.jodi.org/) + [GEO GOO](http://geogoo.net/)

---

### Retour sur le dernier cours

+ [World wide web](https://worldwideweb.cern.ch/), Serveur, client, IP, éditeurs, inspecteur web
+ Structure d'un document HTML (head, body..) et premières balises
+ Mise en ligne sur Netlify

La structure de documents HTML était également visible grâce à inspecteur web de firefox qui, il y a quelques temps encore, proposait une vue 3D de la structure de votre page \(ci-dessous\). Pour visualiser votre page en 3D, vous pouvez aujourd'hui ajouter [Tilt](https://addons.mozilla.org/fr/firefox/addon/tilt/#&gid=1&pid=5) à Firefox \(extension, ou Add-on\).

[![](http://blogzinet.free.fr/images/tilt_3d_mozilla_firefox11_originale.png)](https://addons.mozilla.org/fr/firefox/addon/tilt/#&gid=1&pid=5)

---

### Au programme

* Présences
* Retour sur vos premières pages HTML. Correctifs.
* Créer une page web sans VScode ;)
* Les commentaires et leur fonction
* h1, h2, h3, p, ul, li, a, img, strong, em, blockquote, hr, br
* Inline et block
* Attributs
* [https://developer.mozilla.org/fr/Apprendre/HTML/Introduction\_à\_HTML/Getting\_started](https://developer.mozilla.org/fr/Apprendre/HTML/Introduction_à_HTML/Getting_started)
* [https://learn.shayhowe.com/html-css/building-your-first-web-page/](https://learn.shayhowe.com/html-css/building-your-first-web-page/)
* Articulation HTML/CSS/JS + Architecture d'un site > 
* [Liens, Chemins relatifs et absolus](https://www.w3schools.com/html/html_filepaths.asp). Ancres à l'intérieur d'un document.
* Les liens hypertextes pour naviguer à travers tout ça.
* Feuilles de styles CSS
* Modifiez la couleur de fond de votre page + 1 autre élément html (h1, p, a ...)
* Images et pages web \(RVB, résolution..\)

##### Les élements que nous verrons aujourd'hui:

```html
<a href="#mon_ancre">Aller à mon ancre</a>
    <div id="mon_ancre">
</div>
```

### Au cours:

Sur une thématique choisie, construction de **trois pages HTML** \(ou plus\), **liées entre elles** \(par un menu de navigation\) et reprenant les différents éléments suivants:

1. Plusieurs paragraphes
2. Plusieurs titres \(de différents niveaux\)
3. Un menu de navigation (liste à puces avec liens hypertextes)
4. Au moins une image
5. Un lien sur à image, pointant vers une URL extérieure et 'ouvrant dans un nouvel onglet.

Et pourquoi pas \(facultatif\):

1. Insert d'une video Youtube/Vimeo/Facebook...