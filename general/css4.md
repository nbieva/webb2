---
title: Formulaires + Geonetart
lang: fr-FR
---

# Formulaires + Geonetart

![](/assets/234_deuxvisions.jpg)
[Caroline Delieutraz, Deux visons](http://www.delieutraz.net/fr/deux-visions/)

<a data-fancybox title="Form Art" href="/assets/form-art.png">![Processing](/assets/form-art.png)</a>
<span class="legende">Form Art / Alexei Shulgin / 1997</span>

## Aujourd'hui

+ Retour sur vos **exercices**
+ **Icônes**: Les vôtres, [Font Awesome](https://fontawesome.com/) et [Material icons](https://fonts.google.com/icons)
+ Le **travail de fin d'année**
+ **Formulaires** HTML
+ L'[élément input](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input)
+ L'élément [textarea](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Textarea)
+ Attributs: required, placeholder, disabled, autofocus
+ Styler un formulaire
+ Intégration d'un formulaire (Ex [Google forms](https://docs.google.com/forms/u/0/))
+ Un service pour [envoyer les données de notre formulaire](https://formsubmit.co/) que l'on peut utiliser dans nos templates statiques.
+ Ou, encore plus simple si l'on utilise Netlify, utilisez [Netlify Forms](https://www.netlify.com/products/forms/), que vous trouverez dans l'interface de votre site sur Netlify.
+ media queries

### Formulaires

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Formulaires HTML</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <form name="monformulaire" netlify>
            <label for="nom">Votre nom</label>
            <input type="text" name="nom" placeholder="Inscrivez ici votre nom" required>
            <label for="email">Votre email</label>
            <input type="email" name="email" placeholder="Inscrivez ici votre email" required>
            <label for="message">Votre message</label>
            <textarea name="message"  placeholder="Inscrivez ici votre message" cols="30" rows="10"></textarea>
            <label for="humeur">Votre humeur</label>
            <input type="range" name="humeur">
            <div class="boutons">
                <button type="reset">Reset</button>
                <button type="submit">Send</button>
            </div>
        </form>
    </body>
</html>
```

### Media Queries

+ [Media queries](https://developer.mozilla.org/fr/docs/Web/CSS/Media_Queries/Using_media_queries) + Tester dans le navigateur pour les mobiles
* Quelques règles de base ([Un exemple ici](https://www.alsacreations.com/astuce/lire/1177-Une-feuille-de-styles-de-base-pour-le-Web-mobile.html))

```css
img {
    max-width:100%;
}

@media (width <= 767px) { ... }

@media (767px <= width <= 960px ) { ... }
```

## Websites

+ [https://brutalistwebsites.com/](https://brutalistwebsites.com/)
+ [Désordre - Philippe De Jonckheere](https://www.desordre.net/)
+ [Alexei Shulgin](http://variants.artbase.rhizome.org/Q1249/) + Le travail sur [Rhizome.org](https://rhizome.org/art/artbase/artwork/form-art/)

## Présentation du travail de fin d'année: GEONETART

* Briefing sur digitalab.be: [http://www.digitalab.be/briefing-geonetart-2021/](http://www.digitalab.be/briefing-geonetart-2021/)
* Dates de lectures 
* Remises finales

Envisager le lieu en tant que récit, mémoire, structure, mouvement, interaction..
Concevoir un écho sur le web en prenant en compte les caractéristiques de ce site en ligne \(connectivité au réseau, liens hypertextes, temps réel, diversité des périphériques..\)

N'oubliez pas que ce que permet HTML aujourd'hui va bien au delà de ce qui a été vu au cours\(video, son, animation css..\) sans que cela soit forcément plus compliqué.

Lors de la première lecture du travail :

* Le lieu de référence doit être choisi
* La façon doit le site en ligne fera référence ou dialoguera avec le lieu doit déjà avoir été envisagée.
* Un projet de conception de site doit être réalisé

## Peut-être...

+ [https://fr.qr-code-generator.com/](https://fr.qr-code-generator.com/)
+ https://flawlessapp.io/designtools
+ **Bootstrap**
+ Voir chez[Bootstrap](https://getbootstrap.com/docs/4.4/components/alerts/) aussi...