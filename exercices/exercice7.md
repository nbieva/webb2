---
title: 7. Template HTML-CSS 2
lang: fr-FR
---

# Template HTML-CSS 2


Sur base de ce qui a été vu au cours, téléchargez, modifiez et mettez en ligne sur Netlify un template HTML de votre choix. l'étape de la recherche et de l'analyse est très importante ici.

Le but est ici d'essayer de se retrouver en "situation réelle". Comme si vous deviez faire votre site, celui d'un ami, d'un groupe, d'un media, d'un événement, etc.
A vous de trouver le sujet qui vous convient.

+ Adaptez-le à votre besoin et l'objet de votre site. Vous pouvez pour cela dupliquer, modifier ou supprimer des éléments (images, sliders, textes, ou sections entières..), modifier les couleurs via la ou les feuilles de styles.

**Le but n'est pas ici de faire un "gros" site, mais bien de faire un site "complet"**, où tout ce qui devait être modifié l'a été. Il y a pas mal de demos "Single page" en ligne..

Il vous faut donc trouver un template adapté à la fois à votre besoin, et à votre calendrier..

**Ce travail est à remettre avant le prochain cours. Aucune remise ultérieure ne sera évaluée.**

N'oubliez pas, pour vous y retrouver dans les fichiers (ou identifier un élément) les options de recherche de l'éditeur (Cmd+F au niveau du fichier, ou l'icône de recherche à gauche de l'éditeur au niveau du dossier)

+ Testez.
+ Mettez le tout en ligne sur **[Netlify](https://www.netlify.com/)**. Pour rappel, ce que vous glissez dans la fenêtre de Netlify doit toujours **être un dossier** et **un fichier nommé index.html doit toujours être présent à la racine** de ce dossier.
+ Testez **sur un téléphone ou une tablette**.
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice.