---
title: 4. CSS Zen Garden
lang: fr-FR
---


# CSS Zen Garden

A partir du site de CSS zen Garden et après avoir téléchargé les fichiers HTML et CSS sur ce même site :

+ Créez un nouveau projet en utilisant tel quel leur fichier HTML
+ Ajoutez votre propre feuille de style
+ Au besoin, supprimez le chargement de la leur (pour repartir de zéro si vous le voulez)
+ Essayez de modifier l'apparence de la page en éditant votre feuille de styles. Tentez des choses..
+ Vous devrez bien évidemment pour cela fouiller un peu le web en fonction de ce que vous voulez faire.
+ Mettez votre page en ligne sur **[Netlify](https://www.netlify.com/)**
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice. -->

Essayez de noter sur une feuille de papier (ou dans un fichier) les différentes choses que vous aimeriez faire dans une page web ou les choses que vous voudriez que l'on voit. Vous pouvez pour cela utiliser le pad suivant : [https://pads.domainepublic.net/p/css](https://pads.domainepublic.net/p/css). C'est un document partagé entre nous.

On essaiera de partir de ce que vous aurez noté pour construire les cours suivants.

-----

## Pour vous aider :

+ [https://www.pixelcrea.com/ressources/memo-css3.pdf](https://www.pixelcrea.com/ressources/memo-css3.pdf)
+ [https://jenseign.com/html/wp-content/uploads/2018/01/memo-html-apprendre.pdf](https://jenseign.com/html/wp-content/uploads/2018/01/memo-html-apprendre.pdf)

## Mais aussi :

```css
/* On cible les paragraphes (P) */
p {
    color:#333;
}

/* On cible les éléments SPAN qui sont dans un paragraphe */
p span {
    color:red;
}

/* On cible les paragraphes avec la classe INTRODUCTION */
p.introduction {
    color:blue;
}

/* On cible les éléments (quels qu'ils soient) avec la classe INTRODUCTION */
.introduction {
    /* propriétés */
}
/* On cible les éléments SPAN qui sont dans un élément(quel qu'il soit) avec la classe INTRODUCTION */

.introduction span {
    /* propriétés */
}

/* On cible les liens qui sont dans des LI, qui sont dans des UL (c'est généralement le cas) */
ul li a {
    /* propriétés */
}

/* On cible les liens survolés qui sont dans des LI, qui sont dans des UL */
ul li a:hover {
    /* propriétés */
}
/* On cible les images */
img {
    opacity:0.9;
}

/* On cible les images survolées */
img:hover {
    opacity:1;
}
/* On masque un élément qui a la classe "cacher"(par exemple..) */
.cacher {
    display:none;
}
```





## Base d'une mise en page flexible

Dans le code HTML:

```html
<body>
    <div class="container">
        <!-- insérez ici votre contenu -->
    </div>
</body>
```

Dans le code CSS:

```css
.container {
    width:800px; /*par exemple...*/
    max-width: 100%; /*pour que cette div ne déborde jamais de la page*/
    margin-left:auto;/*pour que la marge extérieure gauche soit flexible*/
    margin-right:auto;/*pour que la marge extérieure droite soit flexible*/
}
img {
    max-width:100%; /*pour que les images ne soient jamais plus larges que leur contenant.*/
    height:auto; /* pour que la hauteur des images s'ajuste à leur largeur (qu'elles gardent leurs proportions) */
}
```

## Plusieurs classes

Si besoin, sachez qu'un élément html peut avoir plusieurs classes. Elles sont alors séparées par un espace dans l'attribut. Comme ceci:

::: warning
Une boîte qui possède deux classes. Une qui définit les paramètres généraux de la boîte (coins arrondis, bordure à gauche...), une autre pour la couleur (color, border-color, background...).
:::

```html
    <div class="custom-block warning">
        Une boîte qui possède deux classes. Une qui définit les paramètres généraux de la boîte, une autre pour la couleur (rouge, jaune ou vert)
    </div>
```