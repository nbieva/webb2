---
title: 1. Hello World!
lang: fr-FR
---

# Hello World!

Ci-dessous une petite vidéo "uncut" qui revient sur les différents éléments à mettre en place pour créer votre page web.

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/1s1VHALagew" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Partie 1

Sur base de ce que nous avons vu au cours, créez une page web sur le sujet de votre choix, avec:

+ Un fichier HTML (index.html)
+ Une structure correcte (head, body..)
+ 2 titres minimum, de  deux niveaux différents (h1, h2, h3, etc.) 
+ Minimum 3 paragraphes, pour avoir un peu de contenu.
+ Une liste à puces (nous n'en avons peut-être pas parlé au cours. A vous de tenter de trouver sur le web les balises HTML pour ceci.)
+ Minimum 3 images dont au moins 1 GIF animé.
+ Modifiez le titre de la page (celui qui s'affiche dans l'onglet de votre navigateur)
+ Testez
+ Envoyez-moi votre dossier (nommé avec vos nom et prénom), zippé, **avant** le prochain cours, dans [ce dossier partagé](https://drive.google.com/drive/folders/1ev2Uuz_oaI97IfHbRqkXvm5tDZ0riDJE?usp=sharing).

## Partie 2

Créez-vous un compte Netlify (Sign up, en haut à droite..): [https://www.netlify.com/](https://www.netlify.com/). Vous pouvez utiliser votre email de l'école pour ceci. Nous détaillerons le fonctionnement de la plateforme au prochain cours.