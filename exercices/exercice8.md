---
title: 8. Formulaires
lang: fr-FR
---

# Formulaires

Créez une page web avec un formulaire centré sur la page

+ Minimum 4-5 champs (veillez à varier les types d'inputs, textarea, select, etc..)
+ Stylez le formulaire
+ Faites en sorte de pouvoir récupérer les données dans l'interface de Netlify (Netlify forms)
+ Mettez en ligne
+ Testez (aussi sur un téléphone ou une tablette).
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice.

**Ce travail est à remettre avant le prochain cours.**

