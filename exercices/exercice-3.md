---
title: 3. Mise en forme
lang: fr-FR
---

# Mise en forme

Sur base de votre exercice précédent, créez un **nouveau** site avec:

+ **Une feuille de style externe (CSS)** liée à toutes vos pages HTML.
+ Minimum **3 règles CSS** (3 éléments différents ciblés) avec **2 propriétés minimum chacune**. Faites vos recherches en fonction de ce que vous voulez modifier.
+ Testez
+ Mettez le tout en ligne sur **[Netlify](https://www.netlify.com/)**. 
+ Ce sera donc votre troisième site dans l'interface de Netlify, les précédents devant rester accessibles. Pour rappel, ce que vous glissez dans la fenêtre de Netlify doit toujours **être un dossier** et **un fichier nommé index.html doit toujours être présent à la racine** de ce dossier.
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice.

## Retour sur Inline, block, div et span

<iframe width="740" height="460" src="https://www.youtube.com/embed/c-Q271lIAsQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Lier une feuille de styles à votre fichier HTML

<iframe width="740" height="460" src="https://www.youtube.com/embed/vmc5V56gzcc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Pour vous aider... 

Les différentes façons de cibler tel ou tel élément de votre page sont reprises ci-dessous. Mais nous verrons cela à notre aise lors du prochain cours.

```css
/* On cible les paragraphes (P) */
p {
    color:#333;
}

/* On cible les éléments SPAN qui sont dans un paragraphe */
p span {
    color:red;
}

/* On cible les paragraphes avec la classe INTRODUCTION */
p.introduction {
    color:blue;
}

/* On cible les éléments (quels qu'ils soient) avec la classe INTRODUCTION */
.introduction {
    /* propriétés */
}
/* On cible les éléments SPAN qui sont dans un élément(quel qu'il soit) avec la classe INTRODUCTION */

.introduction span {
    /* propriétés */
}

/* On cible les liens qui sont dans des LI, qui sont dans des UL (c'est généralement le cas) */
ul li a {
    /* propriétés */
}

/* On cible les liens survolés qui sont dans des LI, qui sont dans des UL */
ul li a:hover {
    /* propriétés */
}
/* On cible les images */
img {
    opacity:0.9;
}

/* On cible les images survolées */
img:hover {
    opacity:1;
}
/* On masque un élément qui a la classe "cacher"(par exemple..) */
.cacher {
    display:none;
}
```