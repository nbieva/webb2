---
title: 3. Mise en forme
lang: fr-FR
---

# Mise en forme

L'adresse doit m'être communiquée avant le prochain cours.

Mettez en forme un clone de votre exercice précédent, avec:

+ Au moins une feuille de style externe chargée dans vos fichiers HTML.
+ Votre mise en forme doit être adaptée au mobiles (testez)
+ Au moins **5 déclarations** CSS différentes (ciblant 5 éléments différents). Chaque déclaration doit comporter au moins **3 règles**.
+ Effet de survol des images obligatoire
+ La navigation doit être mise en forme.
+ Testez
+ Mettez le tout en ligne sur **[Netlify](https://www.netlify.com/)** (Ce sera donc votre troisième site dans l'interface de Netlify). les précédents doivent rester accessibles. Pour rappel, ce que vous glissez dans la fenêtre de Netlify doit toujours **être un dossier** et **un fichier nommé index.html doit toujours être présent à la racine** de ce dossier.
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice.
