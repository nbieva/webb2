---
title: 5. Template HTML-CSS
lang: fr-FR
---

# Template HTML-CSS

![](/assets/html-template.png)

Sur base de ce qui a été vu au cours, téléchargez, modifiez et mettez en ligne sur Netlify le template HTML suivant : [https://html5up.net/lens](https://html5up.net/lens) (téléchargez en haut à droite).

Vous pouvez en choisir [un autre](https://html5up.net/) si vous le désirez mais [celui proposé](https://html5up.net/lens) est relativement simple à prendre en main.

+ **Remplacez les images** par les vôtres (petits formats et grands formats) et modifiez leurs légendes et descriptions.
+ Modifiez les liens des **réseaux sociaux**.
+ Modifiez le **titre** et la **description** de la page et tout autre élément HTML que vous jugeriez nécessaire.
+ Ajoutez au moins une **[police de caractère](https://fonts.google.com/)** supplémentaire.
+ Insérez un **lien vers un PDF** (téléchargement)
+ Testez.
+ Mettez le tout en ligne sur **[Netlify](https://www.netlify.com/)** (Ce sera donc votre quatrième site dans l'interface de Netlify). Les précédents doivent rester accessibles. Pour rappel, ce que vous glissez dans la fenêtre de Netlify doit toujours **être un dossier** et **un fichier nommé index.html doit toujours être présent à la racine** de ce dossier.
+ Testez **sur un téléphone ou une tablette**.
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice.