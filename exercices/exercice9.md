---
title: 9. javascript et P5*js
lang: fr-FR
---

# javascript et P5*js

<video class="videohtml" controls autoplay>
    <source src="/assets/rafman.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video> 

> [John Rafman, Natura morta](http://natura-morta.com/)


+ Créer un **nouveau projet** (dossier)
+ Avoir dans ce projet **une page index.html** simple (avec un titre par exemple) liée à **une feuille de styles** (CSS) externe.
+ **Intégrez** ensuite à ce projet **la bibliothèque [P5js](https://p5js.org/)** ainsi qu'un fichier javascript (.js) dans lequel vous écrirez votre programme.
+ **Copiez-collez** dans ce dernier fichier **un exemple de votre choix** que vous trouverez sur cette page: **[https://p5js.org/examples/](https://p5js.org/examples/)**
+ **Testez**
+ **Mettez en ligne sur Netlify** avant le prochain cours (avant vendredi pour les étudiants du jeudi) et notez l'adresse dans **[notre fichier partagé](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing)**.

C'est à peu de choses près ce qui est décrit dans cette vidéo (à partir de 7'30'' plus ou moins): **[https://www.youtube.com/watch?v=0R3Xt82YIhA](https://www.youtube.com/watch?v=0R3Xt82YIhA)**

Au milieu de cette page, vous trouverez également les différentes manières d'intégrer P5js à votre projet.: **[https://p5js.org/get-started/](https://p5js.org/get-started/)**

Je vous mets aussi ici une petite vidéo sur le passage de Processing à P5js, pour info: **[https://www.youtube.com/watch?v=375SQMe3K7E](https://www.youtube.com/watch?v=375SQMe3K7E)**, ainsi que le lien vers le support du workshop B1 dans lequel nous utilisons à présent P5js: **[https://codep5.netlify.app/](https://codep5.netlify.app/)**

Pour les étudiants du jeudi que je ne verrai malheureusement plus puisque nous manquons le cours de cette semaine, s'il vous plaît, n'hésitez pas à poser des questions! Nous pouvons improviser l'une ou l'utre petite visio si nécessaire.

Pour tous, je reste bien évidemment à votre disposition si vous aviez la moindre question par rapport au travail de fin d'année ou à la matière vue au cours.

**Ce travail est à remettre avant le prochain cours.**
