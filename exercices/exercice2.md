---
title: 2. Premier site web
lang: fr-FR
---

# Premier site web

L'adresse doit m'être communiquée avant le prochain cours.

Sur base de ce que nous avons vu au cours, créez un premier site web sur le sujet de votre choix, avec:

+ Le **dossier de votre site**, organisé avec minimum un sous-dossier (par ex. un sous-dossier "images"...)
+ Au moins **3 pages interconnectées** entre elles (on doit pouvoir accéder à toutes les pages à partir de chacune). L'une d'elle doit bien évidemment être nommée index.html
+ Au moins **une image sur chaque page**. Utilisez des images ayant une taille adaptée au web (par exemple une largeur maximale de 1500px).
+ **Une de vos images doit être un lien** vers un site externe et s'ouvrir dans un nouvel onglet.
+ Testez
+ Mettez le tout en ligne sur **[Netlify](https://www.netlify.com/)** (Voir vidéo ci-dessous). Pour rappel, ce que vous glissez dans la fenêtre de Netlify doit toujours **être un dossier** et **un fichier nommé index.html doit toujours être présent à la racine** de ce dossier.
+ Et enfin, copiez l'adresse de votre page dans [ce fichier](https://docs.google.com/spreadsheets/d/1yCXMbhnxlus8jYzA-Gk9WRd2XeIBvOuoGVNQ4lgmTwg/edit?usp=sharing), à hauteur de votre nom (que vous prendrez soin de noter), et dans la colonne adéquate, correspondant à l'exercice.


<iframe width="740" height="460" src="https://www.youtube.com/embed/Fkeg3egcIx8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Mise en ligne sur Netlify

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/ZFXh7Oyx2rg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
