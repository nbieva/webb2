---
title: Explorations et formulaires
lang: fr-FR
---

# Explorations

<iframe width="740" height="418" src="https://www.youtube.com/embed/7M3PoXQlCvQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Mais encore..

+ [**Responsive design**](https://fr.wikipedia.org/wiki/Site_web_r%C3%A9actif) et [**Media Queries**](https://www.alsacreations.com/article/lire/930-css3-media-queries.html)
+ Création d'[**ombres portées**](https://cssgenerator.org/box-shadow-css-generator.html)
+ [**Geonetart**](http://www.digitalab.be/briefing-geonetart-2021/)
+ [**Formulaires**](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Form)


### Media-Queries

Pour rappel, les **media-queries** vous nous permettre de cibler des contextes particuliers (taille de fenêtre, orientation d'un appareil, etc..) afin d'appliquer des propriétés css spécifiques pour ces contextes.
Les media queries suivent une syntaxe particulière: voir [cette page](https://www.alsacreations.com/article/lire/930-css3-media-queries.html).

```css

/* Notre body sera jaune par défaut */
body {
    background:yellow;
}

/* Notre body sera rouge sur les écrans d'une largeur inférieure à 1250px */
@media (max-width: 1250px) {
    body {
        background:red;
    }
}
```

### Sélecteur universel

Pour rappel aussi, copiez-collez ceci au début de vos feuilles de styles. La propriété [box-sizing](https://developer.mozilla.org/fr/docs/Web/CSS/box-sizing) définit la façon dont la hauteur et la largeur totale d'un élément est calculée.

```css
* {
box-sizing: border-box;
}
```

---

## Quelques sites..

+ [https://brutalistwebsites.com/](https://brutalistwebsites.com/)
+ [Désordre - Philippe De Jonckheere](https://www.desordre.net/)
+ [Alexei Shulgin](http://variants.artbase.rhizome.org/Q1249/) + Le travail sur [Rhizome.org](https://rhizome.org/art/artbase/artwork/form-art/)
+ [Michael Manning, Floating form](http://www.floatingform.net/)


## Formulaires

+ Les éléments de formulaires (voir templates aussi)
+ Les labels
+ Intégration dans Netlify pour collecter des données

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Formulaires HTML</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <form name="monformulaire" netlify>
            <label for="nom">Votre nom</label>
            <input type="text" name="nom" placeholder="Inscrivez ici votre nom" required>
            <label for="email">Votre email</label>
            <input type="email" name="email" placeholder="Inscrivez ici votre email" required>
            <label for="message">Votre message</label>
            <textarea name="message"  placeholder="Inscrivez ici votre message" cols="30" rows="10"></textarea>
            <label for="humeur">Votre humeur</label>
            <input type="range" name="humeur">
            <div class="boutons">
                <button type="reset">Reset</button>
                <button type="submit">Send</button>
            </div>
        </form>
    </body>
</html>
```

## Jusqu'ici...

**Voici un petit résumé, ou mémo, sur ce qui a été vu jusque maintenant:**

+ Créer une **page simple HTML** avec quelques éléments de base (div, span, titres, paragraphes, listes à puces, images, etc..)
+ Comprendre **la syntaxe et la structure d'un document HTML**
+ Savoir créer l'**architecture d'un site** (organisation des fichiers et dossiers)sur son ordinateur par exemple.
+ Savoir naviguer dans cette architecture (remonter d'un dossier, adresses des **liens, relatifs et absolus**, etc..)
+ Savoir mettre ce contenu en ligne, via **Netlify**. 
+ Lier une **feuille de style CSS** externe à un fichier HTML.
+ Comprendre la **syntaxe d'une règle CSS**
+ Comprendre ce que sont les **classes** (attributs HTML).
+ **Savoir les utiliser** dans CSS (savoir comment cibler des éléments de votre fichier html à l'aide de CSS)
+ Comprendre les types **block** et **inline** d'éléments HTML
+ Comprendre comment les modifier via la propriété css **display** (savoir ce que chaque valeur de la propriété implique.. Celles vues au cours du moins)
+ Savoir aligner des éléments les uns par rapport aux autres à l'aide de **Flexbox** et des quelques propriétés vues au cours.
+ Savoir ce que sont **les unités px, %, vw et vh**.
+ Savoir utiliser les sélecteurs CSS repris dans la liste ci-dessous.
+ Donner une **image d'arrière plan** à une page web
+ Charger une (ou plusieurs) nouvelles **polices de caractères** pour votre site web via **Google Fonts**, et les utiliser dans vos CSS.
+ Savoir, bien entendu, **mettre son site à jour**

**Liste de propriété CSS utiles, abordées au cours (faites une recherche sur le web si nécessaire...). En vrac:**

+ display
+ width (+ max-width, min-width)
+ height (+ max-height, min-height)
+ padding (+ padding-left, padding-top, etc..)
+ margin (+ margin-left, margin-top, etc..)
+ border ( (+ border-left, border-top, etc..))
+ background
+ background-color
+ [background-image](https://developer.mozilla.org/fr/docs/Web/CSS/background-image)
+ background-size
+ background-repeat
+ background-position
+ color
+ border-radius
+ text-decoration
+ text-transform
+ opacity
+ justify-content
+ align-items
+ flex-direction
+ font-family
+ font-size
+ font-weight
+ etc.
+ ...
